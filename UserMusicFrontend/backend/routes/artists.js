const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConfig = require('../passport');

const {
    getAllArtists,
    getRandomArtists,
    getArtist,
    getAllSongsArtist,
    postArtist,
    putArtist,
    deleteArtist,
    getAllAttributes
} = require('../controllers/artists');

const { admin } = require('../controllers/users');

router.get('/artists/all', passport.authenticate('jwt', {session: false}), admin, getAllArtists);
router.get('/artists/random/:amountToGet', getRandomArtists);
router.get('/artist/:_id', getArtist);
router.get('/artist/:_id/allSongs', passport.authenticate('jwt', {session: false}), getAllSongsArtist);
router.post('/artist/', passport.authenticate('jwt', {session: false}), admin, postArtist);
router.put('/artist/:_id', passport.authenticate('jwt', {session: false}), admin, putArtist);
router.delete('/artist/:_id', passport.authenticate('jwt', {session: false}), admin, deleteArtist);

router.get('/artists/attributes', getAllAttributes);

module.exports = router;