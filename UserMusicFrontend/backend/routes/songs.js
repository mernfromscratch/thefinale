const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConfig = require('../passport');

const {
    getAllSongs,
    getRandomSongs,
    getSong,
    postSong,
    putSong,
    deleteSong,
    getAllAttributes,
    searchForText,
    increaseCounter
} = require('../controllers/songs');

const { admin } = require('../controllers/users');

router.get('/songs/all', getAllSongs);
router.get('/songs/random/:amountToGet', getRandomSongs);
router.get('/song/:_id', passport.authenticate('jwt', {session: false}), admin, getSong);
router.post('/song/', passport.authenticate('jwt', {session: false}), admin, postSong);
router.put('/song/:_id', passport.authenticate('jwt', {session: false}), admin, putSong);
router.delete('/song/:_id', passport.authenticate('jwt', {session: false}), admin, deleteSong);
router.get('/search/:textToSearchFor', passport.authenticate('jwt', {session: false}), searchForText);
router.put('/song/increasecounter/:_id', passport.authenticate('jwt', {session: false}), increaseCounter);

router.get('/songs/attributes', getAllAttributes);

module.exports = router;