const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConfig = require('../passport');

const {
    getAllPlaylists,
    getMyPlaylists,
    getPlaylistSongs,
    postPlaylist,
    putPlaylist,
    deletePlaylist,
    getAllAttributes,
} = require('../controllers/playlists');

const { admin } = require('../controllers/users');

router.get('/playlists/all', passport.authenticate('jwt', {session: false}), admin, getAllPlaylists);
router.get('/playlists/personal', passport.authenticate('jwt', {session: false}), getMyPlaylists);
router.get('/playlist/:_id/songs', passport.authenticate('jwt', {session: false}), getPlaylistSongs);
router.post('/playlist/', passport.authenticate('jwt', {session: false}), postPlaylist);
router.put('/playlist/:_id', passport.authenticate('jwt', {session: false}), putPlaylist);
router.delete('/playlist/:_id', passport.authenticate('jwt', {session: false}), deletePlaylist);

router.get('/playlists/attributes', getAllAttributes);

module.exports = router;