const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConfig = require('../passport');

const { admin } = require('../controllers/users');

const { uploadImage, removeImage, uploadSong, removeSong } = require('../controllers/cloudinary');

router.post('/uploadimage', passport.authenticate('jwt', {session: false}), admin, uploadImage);
router.delete('/removeimage/:imageId', passport.authenticate('jwt', {session: false}), admin, removeImage);

router.post('/uploadmusic', passport.authenticate('jwt', {session: false}), admin, uploadSong);
router.delete('/removemusic/:musicId', passport.authenticate('jwt', {session: false}), admin, removeSong);

module.exports = router;