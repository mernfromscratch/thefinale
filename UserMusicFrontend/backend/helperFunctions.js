const User = require('./models/User');

exports.makeAdmin = (userName, password) => {
    User.findOne({userName}, (error, user) => {
        if (error) { console.log(error) }
        else if (user) { console.log("Admin already exists") }
        else {
            new User({ userName, password, role: 'admin', age: 110 }).save((error) => {
                if (error) {
                    console.log("Error creating admin");
                }
            });
        };
    });
};