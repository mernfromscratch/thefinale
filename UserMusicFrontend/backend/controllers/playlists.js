const Playlist = require('../models/Playlist');
const Artist = require('../models/Artist');
const User = require('../models/User');
const Song = require('../models/Song');
const _ = require('lodash');
const {
    playlistAttributes
} = require('../constants');

exports.getAllPlaylists = async (req, res) => {
    try {
        const playlists = await Playlist.find({}).exec();
        res.json(playlists);
    } catch (error) {
        res.status(500).json({ message: { text: "Something went wrong", errorOccurred: true, error }});
    };
};

exports.getMyPlaylists = async (req, res) => {
    try {
        const user = await User.findById(req.user._id).populate('playlists').exec();
        if (user.error) { res.status(500).json({ message: "Something went wrong", errorOccurred: true, error: user.error }) };
        res.json(user.playlists);
    } catch (error) {
        res.status(500).json({ message: { text: "Something went wrong", errorOccurred: true, error }});
    };
};

exports.getPlaylistSongs = async (req, res) => {
    try {
        const playlist = await Playlist.findById(req.params._id).exec();
        const songsInPlaylist = await Song.find({ _id: {$in: playlist.songs} }).populate("artists").exec();
        res.json(songsInPlaylist);
    } catch (error) {
        res.status(500).json({ message: { text: "Something went wrong", errorOccurred: true, error }});
    };
};

exports.postPlaylist = async (req, res) => {
    try {
        const userId = req.user._id;
        var user = await User.findById(userId).exec();
        let newPlaylist = { ...req.body, owner: userId };
        const playlist = await new Playlist(newPlaylist).save();
        user.playlists.push(playlist._id);                                                  // Giving _id instead of full playlist avoids callback/infinite loop
        User.findByIdAndUpdate(req.user._id, user).exec();
        res.json(playlist);
    } catch (error) {
        res.status(500).json({ message: { text: "Something went wrong", errorOccurred: true, error }});
    };
};

exports.putPlaylist = async (req, res) => {
    try {
        let playlistCheck = await Playlist.findById(req.params._id).populate('owner', 'userName').populate("songs").exec();
        if (playlistCheck.error) { res.status(500).json(playlistCheck.error) };
        if (!(playlistCheck.owner.userName === req.user.userName)) { res.status(401).json({message: {text: "Not owner", errorOccurred: true}}) };
        const onlyChangingPlaylistInfo = req.body.displaying && (playlistCheck.songs.length === req.body.playlist.songs.length) ;
        if (onlyChangingPlaylistInfo) {
            const playlist = await Playlist.findByIdAndUpdate(req.params._id, req.body.playlist, { new: true })
                .populate({
                    path: "songs",
                    populate: { path: "artists" }
                })
                .exec();
            res.json(playlist);
            return;
        };
        let idsOfPlaylistCheckSongs = playlistCheck.songs.map(playlistCheckSongs => playlistCheckSongs._id.valueOf());
        let songsAreTheSameInNewAndOldPlaylist = false;
        let updatedPlaylist = {};
        let finalUpdatedSongs = [];
        if (req.body.displaying) {                                                  // If request is from inside a playlist then we get updated playlist in body, just need to copy it
            updatedPlaylist = { ...req.body.playlist };
            let idsOfUpdatedPlaylistSongs = updatedPlaylist.songs.map(updatedPlaylistSongs => updatedPlaylistSongs._id);
            songsAreTheSameInNewAndOldPlaylist = _.difference(idsOfPlaylistCheckSongs, idsOfUpdatedPlaylistSongs).length === 0;
        } else {                                                                    // If not from inside a playlist, uses playlist from db and adds song to it
            let newSongs = [...playlistCheck.songs, req.body.song];
            updatedPlaylist = { ...playlistCheck, songs: newSongs };
            songsAreTheSameInNewAndOldPlaylist = idsOfPlaylistCheckSongs.includes(req.body.song._id);
        };
        if (songsAreTheSameInNewAndOldPlaylist) {                                   // If new and old playlist songs are the same, update
            finalUpdatedSongs = playlistCheck.songs;                                // Use playlistcheck's songs, new one might contain double of one song
        } else {                                                                    // If it's not already in playlist, update and send back the updated playlist
            finalUpdatedSongs = updatedPlaylist.songs;
        };
        playlistCheck = await Playlist.findByIdAndUpdate(req.params._id, { songs: finalUpdatedSongs }, { new: true })
            .populate({
                path: "songs",
                populate: { path: "artists" }
            })
            .exec();
        res.json(playlistCheck);
    } catch (error) {
        res.status(500).json({ message: { text: "Something went wrong", errorOccurred: true, error }});
    };
};

exports.deletePlaylist = async (req, res) => {
    try {
        const playlistCheck = await Playlist.findById(req.params._id).populate('owner').exec();
        if (playlistCheck.error) { res.status(500).json(playlistCheck.error) };
        if (!(playlistCheck.owner.userName === req.user.userName)) { res.status(401).json({message: {text: "Not owner", errorOccurred: true}}) };
        const playlist = await Playlist.findByIdAndDelete(req.params._id).exec();
        await User.updateOne({ _id: playlistCheck.owner._id }, {$pull: {playlists: playlistCheck._id} }).exec();
        res.json(playlist);
    } catch (error) {
        res.status(500).json({ message: { text: "Something went wrong", errorOccurred: true, error }});
    };
};

exports.getAllAttributes = async (req, res) => {
    res.json(playlistAttributes);
}
