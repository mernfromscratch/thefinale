const Artist = require('../models/Artist');
const Playlist = require('../models/Playlist');
const User = require('../models/User');
const Song = require('../models/Song');
var ObjectId = require('mongoose').Types.ObjectId;
const {
    artistAttributes
} = require('../constants');

exports.getAllArtists = async (req, res) => {
    try {
        const artists = await Artist.find({}).exec();
        res.json(artists);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getRandomArtists = async (req, res) => {
    try {
        let artists = [];
        const count = await Artist.countDocuments({}).exec();
        if (count <= req.params.amountToGet) { 
            artists = await Artist.find({}).exec();
        } else {
            let amountToSkip = (Math.floor(Math.random() * count)) - req.params.amountToGet;
            artists = await Artist
                .find({})
                .skip(amountToSkip)
                .limit(amountToGet)
                .exec();
        }
        res.json(artists);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getArtist = async (req, res) => {
    try {
        const artist = await Artist.findById(req.params._id).exec();
        res.json(artist);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getAllSongsArtist = async (req, res) => {
    try {
        const artist = await Artist.findById(req.params._id)
            .populate({
                path: "songs",
                populate: [
                    { path: "artists" }
                ]
            })
            .exec();
        res.json(artist.songs);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.postArtist = async (req, res) => {
    try {
        let newArtist = { ...req.body };
        let artist = await new Artist(newArtist).save();
        const idOfSongs = artist.songs.map((song) => song._id);
        await Song.updateMany({ _id: {$in: idOfSongs} }, {
            $push: { artists: new ObjectId(artist._id) }
        }).exec();
        res.json(artist);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.putArtist = async (req, res) => {
    try {
        let updatedArtist = { ...req.body };
        const artist = await (Artist.findByIdAndUpdate(req.params._id, updatedArtist, { new: true })).exec();
        // Delete artist from old songs
        await Song.updateMany({}, {
            $pull: { artists: { _id: artist._id } }
        }).exec();
        // Then add the artist to the new songs
        const idOfSongs = artist.songs.map((song) => song._id);
        await Song.updateMany({ _id: { $in: idOfSongs } }, {
            $push: { artists: new ObjectId(artist._id) }
        }).exec();
        res.json(artist);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.deleteArtist = async (req, res) => {
    try {
        const artist = await Artist.findByIdAndDelete(req.params._id).exec();
        Song.updateMany({}, {
            $pull: {artists: { _id: artist._id }}
        }).exec();
        res.json(artist);            
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getAllAttributes = async (req, res) => {
    res.json(artistAttributes);
}
