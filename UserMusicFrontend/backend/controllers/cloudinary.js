const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});

exports.uploadImage = async (req, res) => {
    try {
        let result = await cloudinary.uploader.upload(req.body.image, {
            public_id: `${Date.now()}`,
            resource_type: 'auto' // jpeg, png etc
        }); 
        res.json({
            publicId: result.public_id,
            url: result.secure_url
        })   
    } catch (error) {
        res.status(500).json({message: { text: error.message, errorOccurred: true, error }});
    }
};
exports.removeImage = async (req, res) => {
    let imageId = req.params.imageId;
    cloudinary.uploader.destroy(imageId, (result) => {
        if (!(result.result === "ok")) return res.status(500).json({message: { text: "Could not destroy", errorOccurred: true, error: result.error }});
        res.status(200).json({message: { text: "Image removed", errorOccurred: false }});
    });
};


exports.uploadSong = async (req, res) => {
    try {
        let result = await cloudinary.v2.uploader.upload(req.body.music, {
            public_id: `${Date.now()}`,
            resource_type: 'video' // wav, mp3 etc
        }); 
        res.json({
            publicId: result.public_id,
            url: result.secure_url
        })   
    } catch (error) {
        res.status(500).json({message: { text: error.message, errorOccurred: true, error }});
    }
};
exports.removeSong = async (req, res) => { 
    let musicId = req.params.musicId;
    cloudinary.v2.uploader.destroy(musicId, {resource_type: 'video'}, (result) => {
        // Bug, successful delete returns undefined result, while error returns result
        if (result) return res.status(500).json({message: { text: "Could not destroy", errorOccurred: true, error: result.error }});
        res.status(200).json({message: { text: "Music removed", errorOccurred: false }});
    }); 
};