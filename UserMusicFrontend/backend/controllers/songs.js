const Playlist = require('../models/Playlist');
const Artist = require('../models/Artist');
const User = require('../models/User');
var ObjectId = require('mongoose').Types.ObjectId;
const Song = require('../models/Song');
const {
    songAttributes
} = require('../constants');

exports.getAllSongs = async (req, res) => {
    try {
        const songs = await Song.find({}).exec();
        res.json(songs);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getRandomSongs = async (req, res) => {
    try {
        let songs = [];
        const count = await Song.countDocuments({}).exec();
        if (count <= req.params.amountToGet) { 
            songs = await Song.find({}).populate("artists").exec();
        } else {
            let amountToSkip = (Math.floor(Math.random() * count)) - req.params.amountToGet;
            songs = await Song
                .find({})
                .skip(amountToSkip)
                .limit(amountToGet)
                .populate("artists")
                .exec();
        }
        res.json(songs);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    };
};

exports.getSong = async (req, res) => {
    try {
        const song = await Song.findById(req.params._id).populate("artists").exec();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.postSong = async (req, res) => {
    try {
        let newSong = { ...req.body };
        const song = await new Song(newSong).save();
        // Push the new song to the relevant artists
        const idOfSongArtists = song.artists.map((art) => art._id);
        await Artist.updateMany({ _id: {$in: idOfSongArtists} }, {
            $push: { songs: new ObjectId(song._id) }
        }).exec();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.putSong = async (req, res) => {
    try {
        const updatedSong = { ...req.body };
        const song = await Song.findByIdAndUpdate(req.params._id, updatedSong, { new: true }).populate("artists").exec();
        // First pull song from all old artists
        await Artist.updateMany({}, {
            $pull: { songs: new ObjectId(song._id) }
        }).exec();
        const idOfSongArtists = song.artists.map((art) => art._id);
        // Then push to the new relevant artists
        await Artist.updateMany({ _id: {$in: idOfSongArtists} }, {
            $push: { songs: new ObjectId(song._id) }
        }).exec();
        res.json(song);
        // Something like the below would also work for pushing the song to the artists
        // song.artists.forEach((artist) => {
        //     if (!artist.songs.includes(artist)) {
        //         artist.songs.push(artist);
        //         Artist.findByIdAndUpdate(artist._id, artist).exec();
        //     };
        // });
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.deleteSong = async (req, res) => {
    try {
        const song = await Song.findByIdAndDelete(req.params._id).exec();
        Artist.updateMany({}, {
            $pull: {songs: { _id: song._id }}
        }).exec();
        Playlist.updateMany({}, {
            $pull: {songs: { _id: song._id }}
        }).exec();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.getAllAttributes = async (req, res) => {
    res.json(songAttributes);
}

exports.searchForText = async (req, res) => {
    try {
        let searchString = req.params.textToSearchFor;
        const songs = await Song.find({ title: {$regex: `${searchString}`, $options: 'i'} }).exec();
        const artists = await Artist.find({ name: {$regex: `${searchString}`, $options: 'i'} }).exec();
        res.json({ songs, artists });
    } catch (error) {
        res.status(500).json(error);
    };
};

exports.increaseCounter = async (req, res) => {
    try {
        const idOfSong = req.params._id;
        const song = await Song.findByIdAndUpdate(idOfSong, {
            $inc: { timesHeard: 1 }
        }, {new: true}).exec();
        res.json(song);
    } catch (error) {
        res.status(500).json(error);
    }
}