const mongoose = require('mongoose');
const ObjectId = mongoose.ObjectId;

const ArtistSchema = mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    details: {
        type: String,
        default: ""
    },
    songs: [{
        type: ObjectId,
        ref: 'Song'
    }],
    pictureUrl: String,
    imageId: String
});

module.exports = mongoose.model('Artist', ArtistSchema);