const mongoose = require('mongoose');
const ObjectId = mongoose.ObjectId;

const PlaylistSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    songs: [{
        type: ObjectId,
        ref: 'Song'
    }],
    owner: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model('Playlist', PlaylistSchema);