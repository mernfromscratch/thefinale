const mongoose = require('mongoose');
const ObjectId = mongoose.ObjectId;
const {
    artistOrBandNames, 
    genres 
} =  require('../constants');

const SongSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minLength: 1,
        unique: true
    },
    length: {
        type: Number,
        min: 1,
        max: 72000,
        required: true
    },
    genre: {
        type: String,
        enum: genres,
        default: "Unknown"
    },
    artists: [{
        type: ObjectId,
        ref: 'Artist'
    }],
    songUrl: {
        type: String,
        unique: true
    },
    cloudinarySongId: {
        type: String,
        unique: true
    },
    timesHeard: {
        type: Number,
        min: 0,
        default: 0 
    },
    exclusive: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Song', SongSchema);