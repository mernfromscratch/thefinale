exports.genres = ["Metal", "Country", "Pop", "Classic", "New Age", "Rock", "Electro", "Unknown"];

exports.songAttributes = [
    {displayName: "Title", valueName: "title", type: "text", adminOnly: false, default: ""},
    {displayName: "Length", valueName: "length", type: "number", min: 1, max: 72000, default: 60, doNotShowInput: true},
    {displayName: "Genre", valueName: "genre", type: "select", options: this.genres, multiple: false, default: "Unknown"},
    {displayName: "Artists", valueName: "artists", type: "modelSelect", modelOptions: "artists", multiple: true, default: []},
    {displayName: "Times heard", valueName: "timesHeard", type: "number", default: 0, min: 0, doNotShowInput: true},
    {displayName: "Exclusive", valueName: "exclusive", type: "checkbox", adminOnly: true, default: false},
    {displayName: "Song URL", valueName: "songUrl", type: "music", defaultValue: "", onlyForCreateOrEdit: true},
];

exports.userAttributes = [
    {displayName: "Your password", valueName: "password", type: "password", defaultValue: "", onlyForMakingAdmin: true},
    {displayName: "User name", valueName: "userName", type: "text", defaultValue: "", onlyForRegister: true},
    {displayName: "Password", valueName: "password", type: "password", defaultValue: "", onlyForRegister: true},
    {displayName: "First name", valueName: "firstName", type: "text", defaultValue: ""},
    {displayName: "Last name", valueName: "lastName", type: "text", defaultValue: ""},
    {displayName: "Age", valueName: "age", type: "number", min: 18, defaultValue: 25},
];

exports.playlistAttributes = [
    {displayName: "Name", valueName: "name", type: "text", defaultValue: ""}
];

exports.artistAttributes = [
    {displayName: "Name", valueName: "name", type: "text", defaultValue: ""},
    {displayName: "Details", valueName: "details", type: "textarea", defaultValue: ""},
    {displayName: "Songs", valueName: "songs", type: "modelSelect", modelOptions: "songs", multiple: true, default: []},
    {displayName: "Picture URL", valueName: "pictureUrl", type: "picture", defaultValue: ""},
];