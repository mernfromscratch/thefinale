import {Route, Switch, useHistory} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { checkIfAuthenticatedUserAction } from './redux/actions/authActions';
import { getPersonalPlaylistsAction } from './redux/actions/songActions';
import './App.css';

import Toaster from './components/Toaster';
import NavBar from './components/NavBar';
import Home from './components/Home';

import Playlists from './components/song/Playlists';
import Playlist from './components/song/Playlist';
import PlayBar from './components/song/PlayBar';

import UserRoute from './components/routes/UserRoute';
import Logout from './components/routes/Logout';
import UserForm from './components/cards/UserForm';
import LoginForm from './components/cards/LoginForm';
import ChooseItemToEdit from './components/cards/ChooseItemToEdit';
import ChangePasswordForm from './components/cards/ChangePasswordForm';
import ArtistPage from './components/song/ArtistPage';
import SearchPage from './components/song/SearchPage';

import AdminRoute from './components/routes/AdminRoute';
import AdminPage from './components/admin/AdminPage';
import ItemEdit from './components/cards/ItemEdit';
import ItemCreate from './components/cards/ItemCreate';
import { getRandomArtistInfo } from './helperFunctions/song';

function App() {

  const dispatch = useDispatch();
  const history = useHistory();
  const { songState, authState } = useSelector(state => state);
  const [ randomMusicalInfo, setRandomMusicalInfo ] = useState(null);
  const [ randomArtistName, setRandomArtistName ] = useState(null);
  const [showRegister, setShowRegister] = useState(false);

  useEffect(() => {
    dispatch(checkIfAuthenticatedUserAction());
  }, []);

  useEffect(() => {
    authState.authenticated && dispatch(checkIfAuthenticatedUserAction());
    !authState.authenticated && setShowRegister(true);
    !authState.authenticated && history.push("/");
  }, [songState.loading]);

  useEffect(() => {
    if (songState.artists.length > 0) {
      const randomArtistsName = (songState.artists[ Math.floor(Math.random() * songState.artists.length) ]).name;
      setRandomArtistName(randomArtistsName);
      getRandomArtistInfo(randomArtistsName).then(artistInfo => setRandomMusicalInfo(artistInfo)).catch(e => {});
    };
  }, [songState.artists]);

  useEffect(() => {
    authState.authenticated && dispatch(getPersonalPlaylistsAction());
  }, [authState.authenticated]);

  return (
    <div className="App w-100 h-100 mb-n5 d-flex flex-column hideOverflowX">
      <NavBar />
      <Toaster />
      <div className="h-100 overflow-hidden">
        <div className={showRegister ? "h-100" : "h-100 row g-0"}>
          { authState.authenticated &&
              <div className="col-2 pt-5 p-3 themeColors h-100">
                <Playlists />
              </div>
          }
          <div className={authState.authenticated ? "h-100 col-8 overflow-auto" : "h-100"}>
              <Switch>
                <Route exact path='/'> <Home showRegister={showRegister} setShowRegister={setShowRegister} /> </ Route>
                <Route exact path='/user/register' component={UserForm} />
                <Route exact path='/user/login' component={LoginForm} />

                <UserRoute exact path='/user/passwordChange' component={ChangePasswordForm} />
                <UserRoute exact path='/user/edit' component={UserForm} />
                <UserRoute exact path='/user/logout' component={Logout} />
                <UserRoute exact path='/playlist/:_id' component={Playlist} />
                <UserRoute exact path='/artist/:_id' component={ArtistPage} />
                <UserRoute exact path='/search' component={SearchPage} />

                <AdminRoute exact path='/admin' component={AdminPage} />
                <AdminRoute exact path='/admin/create' component={UserForm} />
                <AdminRoute exact path='/create/artist' component={ItemCreate} />
                <AdminRoute exact path='/create/song' component={ItemCreate} />
                <AdminRoute exact path='/edit/' component={ChooseItemToEdit} />
                <AdminRoute exact path='/edit/artist/:_id' component={ItemEdit} />
                <AdminRoute exact path='/edit/song/:_id' component={ItemEdit} />
              </Switch>
          </div>
          { authState.authenticated && 
              <div className="col-2 text-center d-flex align-items-center pt-5 p-1 px-5 themeColors">
                <div className="container fst-italic">
                  <div className="row-2 pb-3">
                    { randomMusicalInfo && randomArtistName }
                  </div>
                  <div className="row-6">
                  { randomMusicalInfo && randomMusicalInfo.split(".").slice(0, 2).join(".") }
                  </div>
                </div>
              </div>
          }
        </div>
      </div>
      { authState.authenticated &&  
        <PlayBar />
      }
    </div>
  );
};

export default App;
