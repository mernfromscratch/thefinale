import axios from 'axios';

export const getSong = async (_id) => {
    try {
        const song = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/song/${_id}`);
        return song.data;
    } catch (error) {
        return {};
    }
};

export const getArtist = async (_id) => {
    try {
        const artist = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/artist/${_id}`);
        return artist.data;
    } catch (error) {
        return {};
    }
};

export const getRandomArtistInfo = async (name) => {
    try {
        const randomArtists = await axios.get(`https://api.discogs.com/database/search?type=artist&q=${name}&token=${process.env.REACT_APP_RANDOM_ARTIST_INFO_TOKEN}`);
        const randomArtistInfo = await axios.get(randomArtists.data.results[0].resource_url);
        return randomArtistInfo.data.profile;
    } catch (error) {
        return null;
    }
};

export const getSongAttributes = async () => {
    try {
        const attributes = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/songs/attributes`);
        return attributes.data;
    } catch (error) {
        return [];
    }
};

export const getPlaylistAttributes = async () => {
    try {
        const attributes = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/playlists/attributes`);
        return attributes.data;
    } catch (error) {
        return [];
    }
};

export const getArtistAttributes = async () => {
    try {
        const attributes = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/artists/attributes`);
        return attributes.data;
    } catch (error) {
        return [];
    }
};

export const sortSongs = (songA, songB) => {
    if (songA.title < songB.title) {return -1}
    else if (songA.title > songB.title) {return 1}
    else {return 0};
};

export const sortArtistsOrPlaylists = (itemA, itemB) => {
    if (itemA.name < itemB.name) {return -1}
    else if (itemA.name > itemB.name) {return 1}
    else {return 0};
};

export const makeTimeHumanFriendly = (time) => {
    if (time < 10) { return `0:0${time}` }
    else { return `${Math.floor(time/60)}:${time%60}` };
};

export const makeErrorMessage = (messageText, error) => {
    return { text: messageText, errorOccurred: true, error };
};