import {
    GET_ALL_SONGS, GET_ALL_SONGS_SUCCESS, GET_ALL_SONGS_ERROR,
    GET_RANDOM_SONGS, GET_RANDOM_SONGS_SUCCESS, GET_RANDOM_SONGS_ERROR,
    GET_SONG_EDITING, GET_SONG_EDITING_SUCCESS, GET_SONG_EDITING_ERROR,
    POST_SONG, POST_SONG_SUCCESS, POST_SONG_ERROR,
    PUT_SONG, PUT_SONG_SUCCESS, PUT_SONG_ERROR,
    DELETE_SONG, DELETE_SONG_SUCCESS, DELETE_SONG_ERROR,
    RESET_SONG_FOR_EDITING, RESET_SONG_FOR_EDITING_SUCCESS, 
    RESET_MESSAGE_SONG, RESET_MESSAGE_SONG_SUCCESS, 
    CONTINUE_SONG, CONTINUE_SONG_SUCCESS, 
    PLAY_NEW_SONG, PLAY_NEW_SONG_SUCCESS, 
    PAUSE_SONG, PAUSE_SONG_SUCCESS, 
    PLAY_NEXT_SONG, PLAY_NEXT_SONG_SUCCESS, 
    PLAY_PREVIOUS_SONG, PLAY_PREVIOUS_SONG_SUCCESS, 

    GET_ALL_ARTISTS, GET_ALL_ARTISTS_SUCCESS, GET_ALL_ARTISTS_ERROR,
    GET_RANDOM_ARTISTS, GET_RANDOM_ARTISTS_SUCCESS, GET_RANDOM_ARTISTS_ERROR,
    GET_ARTIST_SONGS, GET_ARTIST_SONGS_SUCCESS, GET_ARTIST_SONGS_ERROR,
    POST_ARTIST, POST_ARTIST_SUCCESS, POST_ARTIST_ERROR,
    PUT_ARTIST, PUT_ARTIST_SUCCESS, PUT_ARTIST_ERROR,
    DELETE_ARTIST, DELETE_ARTIST_SUCCESS, DELETE_ARTIST_ERROR,
    GET_ARTIST_EDITING, GET_ARTIST_EDITING_SUCCESS, GET_ARTIST_EDITING_ERROR,
    RESET_ARTIST_FOR_EDITING, RESET_ARTIST_FOR_EDITING_SUCCESS, 

    GET_ALL_PLAYLISTS, GET_ALL_PLAYLISTS_SUCCESS, GET_ALL_PLAYLISTS_ERROR,
    GET_PERSONAL_PLAYLISTS, GET_PERSONAL_PLAYLISTS_SUCCESS, GET_PERSONAL_PLAYLISTS_ERROR,
    GET_PLAYLIST_SONGS, GET_PLAYLIST_SONGS_SUCCESS, GET_PLAYLIST_SONGS_ERROR,
    POST_PLAYLIST, POST_PLAYLIST_SUCCESS, POST_PLAYLIST_ERROR,
    PUT_PLAYLIST, PUT_PLAYLIST_SUCCESS, PUT_PLAYLIST_ERROR,
    DELETE_PLAYLIST, DELETE_PLAYLIST_SUCCESS, DELETE_PLAYLIST_ERROR,

    UPLOAD_IMAGE, UPLOAD_IMAGE_ERROR, UPLOAD_IMAGE_SUCCESS,
    REMOVE_IMAGE, REMOVE_IMAGE_SUCCESS, REMOVE_IMAGE_ERROR, 
    UPLOAD_MUSIC, UPLOAD_MUSIC_ERROR, UPLOAD_MUSIC_SUCCESS,
    REMOVE_MUSIC, REMOVE_MUSIC_SUCCESS, REMOVE_MUSIC_ERROR,

    INCREASE_COUNTER,
    EDIT_SEARCH_TEXT, EDIT_SEARCH_TEXT_SUCCESS, 
    SEARCH, SEARCH_SUCCESS, SEARCH_ERROR
} from './songActionTypes';

import {
    getAllSongs,
    getRandomSongs,
    getSong,
    postSong,
    putSong,
    deleteSong,
    
    getAllArtists,
    getRandomArtists,
    getArtistSongs,
    getArtist,
    postArtist,
    putArtist,
    deleteArtist,
    
    getAllPlaylists,
    getPersonalPlaylists,
    postPlaylist,
    putPlaylist,
    deletePlaylist,
    
    uploadImage,
    removeImage,
    uploadMusic,
    removeMusic,

    search,
    increaseCounter,
    getPlaylistSongs
} from '../api/song';

export const getAllSongsAction = () => async dispatch => {
    dispatch({ type: GET_ALL_SONGS });
    getAllSongs()
        .then((songs) => {
            dispatch({
                type: GET_ALL_SONGS_SUCCESS,
                payload: songs 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_ALL_SONGS_ERROR,
                payload: error.response.data
            });
        });
};

export const getRandomSongsAction = (amountToGet) => async dispatch => {
    dispatch({ type: GET_RANDOM_SONGS });
    getRandomSongs(amountToGet)
        .then((songs) => {
            dispatch({
                type: GET_RANDOM_SONGS_SUCCESS,
                payload: songs 
            });
        })
        .catch((error) => {
            console.log(error);
            dispatch({
                type: GET_RANDOM_SONGS_ERROR,
                payload: error.response.data
            });
        });
};

export const getSongForEditAction = (_id) => async dispatch => {
    dispatch({ type: GET_SONG_EDITING });
    getSong(_id)
        .then((songFromBack) => {
            dispatch({
                type: GET_SONG_EDITING_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_SONG_EDITING_ERROR,
                payload: error.response.data
            });
        });
};

export const postSongAction = (song) => async dispatch => {
    dispatch({ type: POST_SONG });
    postSong(song)
        .then((songFromBack) => {
            dispatch({
                type: POST_SONG_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: POST_SONG_ERROR,
                payload: error.response.data
            });
        });
};

export const putSongAction = (song) => async dispatch => {
    dispatch({ type: PUT_SONG });
    putSong(song)
        .then((songFromBack) => {
            dispatch({
                type: PUT_SONG_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: PUT_SONG_ERROR,
                payload: error.response.data
            });
        });
};

export const deleteSongAction = (_id) => async dispatch => {
    dispatch({ type: DELETE_SONG });
    deleteSong(_id)
        .then((songFromBack) => {
            dispatch({
                type: DELETE_SONG_SUCCESS,
                payload: songFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: DELETE_SONG_ERROR,
                payload: error.response.data
            });
        });
};

export const resetSongForEditingAction = () => async dispatch => {
    dispatch({ type: RESET_SONG_FOR_EDITING });
    dispatch({
        type: RESET_SONG_FOR_EDITING_SUCCESS,
        payload: {}
    });
};

export const resetSongMessageAction = () => async dispatch => {
    dispatch({ type: RESET_MESSAGE_SONG });
    dispatch({
        type: RESET_MESSAGE_SONG_SUCCESS,
        payload: {}
    });
};

export const continueSongAction = () => async dispatch => {
    dispatch({ type: CONTINUE_SONG });
    dispatch({
        type: CONTINUE_SONG_SUCCESS,
        payload: {}
    });
};

export const playNewSongAction = (songToPlay) => async dispatch => {
    dispatch({ type: PLAY_NEW_SONG });
    dispatch({
        type: PLAY_NEW_SONG_SUCCESS,
        payload: songToPlay
    });
};

export const pauseSongAction = () => async dispatch => {
    dispatch({ type: PAUSE_SONG });
    dispatch({
        type: PAUSE_SONG_SUCCESS,
        payload: {}
    });
};

export const playNextSongAction = () => async dispatch => {
    dispatch({ type: PLAY_NEXT_SONG });
    dispatch({
        type: PLAY_NEXT_SONG_SUCCESS,
        payload: {}
    });
};

export const playPreviousSongAction = () => async dispatch => {
    dispatch({ type: PLAY_PREVIOUS_SONG });
    dispatch({
        type: PLAY_PREVIOUS_SONG_SUCCESS,
        payload: {}
    });
};

//////////////////////////////////////////////////////////////////////

export const getAllArtistsAction = () => async dispatch => {
    dispatch({ type: GET_ALL_ARTISTS });
    getAllArtists()
        .then((artists) => {
            dispatch({
                type: GET_ALL_ARTISTS_SUCCESS,
                payload: artists 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_ALL_ARTISTS_ERROR,
                payload: error.response.data
            });
        });
};

export const getRandomArtistsAction = (amountToGet) => async dispatch => {
    dispatch({ type: GET_RANDOM_ARTISTS });
    getRandomArtists(amountToGet)
        .then((artists) => {
            dispatch({
                type: GET_RANDOM_ARTISTS_SUCCESS,
                payload: artists 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_RANDOM_ARTISTS_ERROR,
                payload: error.response.data
            });
        });
};

export const getArtistSongsAction = (_id) => async dispatch => {
    dispatch({ type: GET_ARTIST_SONGS });
    getArtistSongs(_id)
        .then((songs) => {
            dispatch({
                type: GET_ARTIST_SONGS_SUCCESS,
                payload: songs 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_ARTIST_SONGS_ERROR,
                payload: error.response.data
            });
        });
};

export const getArtistForEditingAction = (_id) => async dispatch => {
    dispatch({ type: GET_ARTIST_EDITING });
    getArtist(_id)
        .then((artistFromBack) => {
            dispatch({
                type: GET_ARTIST_EDITING_SUCCESS,
                payload: artistFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_ARTIST_EDITING_ERROR,
                payload: error.response.data
            });
        });
};

export const postArtistAction = (artist) => async dispatch => {
    dispatch({ type: POST_ARTIST });
    postArtist(artist)
        .then((artistFromBack) => {
            dispatch({
                type: POST_ARTIST_SUCCESS,
                payload: artistFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: POST_ARTIST_ERROR,
                payload: error.response.data
            });
        });
};

export const putArtistAction = (artist) => async dispatch => {
    dispatch({ type: PUT_ARTIST });
    putArtist(artist)
        .then((artistFromBack) => {
            dispatch({
                type: PUT_ARTIST_SUCCESS,
                payload: artistFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: PUT_ARTIST_ERROR,
                payload: error.response.data
            });
        });
};

export const deleteArtistAction = (_id) => async dispatch => {
    dispatch({ type: DELETE_ARTIST });
    deleteArtist(_id)
        .then((artistFromBack) => {
            dispatch({
                type: DELETE_ARTIST_SUCCESS,
                payload: artistFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: DELETE_ARTIST_ERROR,
                payload: error.response.data
            });
        });
};

export const resetArtistForEditingAction = () => async dispatch => {
    dispatch({ type: RESET_ARTIST_FOR_EDITING });
    dispatch({
        type: RESET_ARTIST_FOR_EDITING_SUCCESS,
        payload: {}
    });
};

//////////////////////////////////////////////////////////////////////


export const getAllPlaylistsAction = () => async dispatch => {
    dispatch({ type: GET_ALL_PLAYLISTS });
    getAllPlaylists()
        .then((playlists) => {
            dispatch({
                type: GET_ALL_PLAYLISTS_SUCCESS,
                payload: playlists 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_ALL_PLAYLISTS_ERROR,
                payload: error.response.data
            });
        });
};

export const getPersonalPlaylistsAction = () => async dispatch => {
    dispatch({ type: GET_PERSONAL_PLAYLISTS });
    getPersonalPlaylists()
        .then((playlists) => {
            dispatch({
                type: GET_PERSONAL_PLAYLISTS_SUCCESS,
                payload: playlists 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_PERSONAL_PLAYLISTS_ERROR,
                payload: error.response.data
            });
        });
};

export const getPlaylistSongsAction = (_id) => async dispatch => {
    dispatch({ type: GET_PLAYLIST_SONGS });
    getPlaylistSongs(_id)
        .then((playlistSongsFromBack) => {
            dispatch({
                type: GET_PLAYLIST_SONGS_SUCCESS,
                payload: playlistSongsFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: GET_PLAYLIST_SONGS_ERROR,
                payload: error.response.data
            });
        });
};

export const postPlaylistAction = (playlist) => async dispatch => {
    dispatch({ type: POST_PLAYLIST });
    postPlaylist(playlist)
        .then((playlistFromBack) => {
            dispatch({
                type: POST_PLAYLIST_SUCCESS,
                payload: playlistFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: POST_PLAYLIST_ERROR,
                payload: error.response.data
            });
        });
};

export const putPlaylistAction = (playlist, song, displaying) => async dispatch => {
    dispatch({ type: PUT_PLAYLIST });
    putPlaylist(playlist, song, displaying)
        .then((playlistFromBack) => {
            dispatch({
                type: PUT_PLAYLIST_SUCCESS,
                payload: { playlistFromBack } 
            });
        })
        .catch((error) => {
            dispatch({
                type: PUT_PLAYLIST_ERROR,
                payload: error.response.data
            });
        });
};

export const deletePlaylistAction = (_id) => async dispatch => {
    dispatch({ type: DELETE_PLAYLIST });
    deletePlaylist(_id)
        .then((playlistFromBack) => {
            dispatch({
                type: DELETE_PLAYLIST_SUCCESS,
                payload: playlistFromBack 
            });
        })
        .catch((error) => {
            dispatch({
                type: DELETE_PLAYLIST_ERROR,
                payload: error.response.data
            });
        });
};

//////////////////////////////////////////////////////////////////////

export const uploadImageAction = (image) => async dispatch => {
    dispatch({ type: UPLOAD_IMAGE });
    uploadImage(image)
        .then((cloudinaryData) => {
            dispatch({
                type: UPLOAD_IMAGE_SUCCESS,
                payload: cloudinaryData
            });
        })
        .catch((error) => {
            dispatch({
                type: UPLOAD_IMAGE_ERROR,
                payload: error.response.data
            });
        });
};

export const removeImageAction = (idOfPicture) => async dispatch => {
    dispatch({ type: REMOVE_IMAGE });
    removeImage(idOfPicture)
        .then((response) => {
            dispatch({
                type: REMOVE_IMAGE_SUCCESS,
                payload: idOfPicture
            });
        })
        .catch((error) => {
            dispatch({
                type: REMOVE_IMAGE_ERROR,
                payload: error.response.data
            });
        });
};

export const uploadMusicAction = (music) => async dispatch => {
    dispatch({ type: UPLOAD_MUSIC });
    uploadMusic(music)
        .then((cloudinaryData) => {
            dispatch({
                type: UPLOAD_MUSIC_SUCCESS,
                payload: cloudinaryData
            });
        })
        .catch((error) => {
            dispatch({
                type: UPLOAD_MUSIC_ERROR,
                payload: error.response.data
            });
        });
};

export const removeMusicAction = (idOfMusic) => async dispatch => {
    dispatch({ type: REMOVE_MUSIC });
    removeMusic(idOfMusic)
        .then((response) => {
            dispatch({
                type: REMOVE_MUSIC_SUCCESS,
                payload: idOfMusic
            });
        })
        .catch((error) => {
            dispatch({
                type: REMOVE_MUSIC_ERROR,
                payload: error.response.data
            });
        });
};

//////////////////////////////////////////////////////////////////////

export const increaseCounterAction = (_id) => async dispatch => {
    dispatch({ type: INCREASE_COUNTER });
    increaseCounter(_id)
        .then((response) => {})
        .catch((error) => {console.log(error)});
};

export const editSearchTextAction = (searchText) => async dispatch => {
    dispatch({ type: EDIT_SEARCH_TEXT });
    dispatch({
        type: EDIT_SEARCH_TEXT_SUCCESS,
        payload: searchText
    });
};


export const searchAction = (textToSearchFor) => async dispatch => {
    dispatch({ type: SEARCH });
    search(textToSearchFor)
        .then((response) => {
            dispatch({
                type: SEARCH_SUCCESS,
                payload: response
            });
        })
        .catch((error) => {
            dispatch({
                type: SEARCH_ERROR,
                payload: error.response.data
            });
        });
};
