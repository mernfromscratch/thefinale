import {
    REGISTER_USER, REGISTER_USER_SUCCESS, REGISTER_USER_ERROR,
    LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_ERROR,
    LOGOUT_USER, LOGOUT_USER_SUCCESS, LOGOUT_USER_ERROR,
    CHECK_IF_AUTHENTICATED_USER, CHECK_IF_AUTHENTICATED_USER_SUCCESS, CHECK_IF_AUTHENTICATED_USER_ERROR,
    GET_CURRENT_USER, GET_CURRENT_USER_SUCCESS, GET_CURRENT_USER_ERROR,
    CLEAR_EDIT_USER_INFO, CLEAR_EDIT_USER_INFO_SUCCESS, 
    CLEAR_USER_MESSAGE, CLEAR_USER_MESSAGE_SUCCESS,
    PUT_USER, PUT_USER_SUCCESS, PUT_USER_ERROR,
    PUT_PASSWORD, PUT_PASSWORD_SUCCESS, PUT_PASSWORD_ERROR,
    POST_ADMIN, POST_ADMIN_SUCCESS, POST_ADMIN_ERROR
} from './authActionTypes';

import {
    register,
    login,
    logout,
    isAuthenticated,
    getCurrentUser,
    putUser,
    putPassword,
    postAdmin
} from '../api/auth';

export const registerUserAction = (user) => async dispatch => {
    dispatch({ type: REGISTER_USER });
    register(user)
        .then(user => dispatch({
            type: REGISTER_USER_SUCCESS,
            payload: user
        }))
        .catch(error => dispatch({
            type: REGISTER_USER_ERROR,
            payload: error.response.data
        }));
};

export const loginUserAction = (loginCredentials) => async dispatch => {
    dispatch({ type: LOGIN_USER });
    login(loginCredentials.userName, loginCredentials.password)
        .then(response => dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: response
        }))
        .catch(error => dispatch({
            type: LOGIN_USER_ERROR,
            payload: error.response.data
        }));
};

export const logoutUserAction = () => async dispatch => {
    dispatch({ type: LOGOUT_USER });
    logout()
        .then(response => dispatch({
            type: LOGOUT_USER_SUCCESS,
            payload: response
        }))
        .catch(error => dispatch({
            type: LOGOUT_USER_ERROR,
            payload: error.response.data
        }));
};

export const checkIfAuthenticatedUserAction = () => async dispatch => {
    dispatch({ type: CHECK_IF_AUTHENTICATED_USER });
    isAuthenticated()
        .then(response => dispatch({
            type: CHECK_IF_AUTHENTICATED_USER_SUCCESS,
            payload: response
        }))
        .catch(error => dispatch({
            type: CHECK_IF_AUTHENTICATED_USER_ERROR,
            payload: error.response.data
        }));
};

export const getCurrentUserAction = () => async dispatch => {
    dispatch({ type: GET_CURRENT_USER });
    getCurrentUser()
        .then(response => dispatch({
            type: GET_CURRENT_USER_SUCCESS,
            payload: response
        }))
        .catch(error => dispatch({
            type: GET_CURRENT_USER_ERROR,
            payload: error.response.data
        }));
};

export const clearCurrentUserInfoAction = () => async dispatch => {
    dispatch({ type: CLEAR_EDIT_USER_INFO });
    dispatch({
            type: CLEAR_EDIT_USER_INFO_SUCCESS,
            payload: {}
        });
};

export const clearUserMessageAction = () => async dispatch => {
    dispatch({ type: CLEAR_USER_MESSAGE });
    dispatch({
            type: CLEAR_USER_MESSAGE_SUCCESS,
            payload: {}
        });
};

export const putUserAction = (user) => async dispatch => {
    dispatch({ type: PUT_USER });
    putUser(user)
        .then(response => dispatch({
            type: PUT_USER_SUCCESS,
            payload: {...response, currentUserToEdit: user}
        }))
        .catch(error => dispatch({
            type: PUT_USER_ERROR,
            payload: error.response.data
        }));
};

export const putPasswordAction = (passwordOldAndNew) => async dispatch => {
    dispatch({ type: PUT_PASSWORD });
    putPassword(passwordOldAndNew)
        .then(response => dispatch({
            type: PUT_PASSWORD_SUCCESS,
            payload: response
        }))
        .catch(error => dispatch({
            type: PUT_PASSWORD_ERROR,
            payload: error.response.data
        }));
};

export const postAdminAction = (newAdmin, password) => async dispatch => {
    dispatch({ type: POST_ADMIN });
    postAdmin(newAdmin, password)
        .then(response => dispatch({
            type: POST_ADMIN_SUCCESS,
            payload: response
        }))
        .catch(error => dispatch({
            type: POST_ADMIN_ERROR,
            payload: error.response.data
        }));
};