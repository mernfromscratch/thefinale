import {
    GET_ALL_SONGS, GET_ALL_SONGS_SUCCESS, GET_ALL_SONGS_ERROR,
    GET_RANDOM_SONGS, GET_RANDOM_SONGS_SUCCESS, GET_RANDOM_SONGS_ERROR,
    GET_SONG_EDITING, GET_SONG_EDITING_SUCCESS, GET_SONG_EDITING_ERROR,
    POST_SONG, POST_SONG_SUCCESS, POST_SONG_ERROR,
    PUT_SONG, PUT_SONG_SUCCESS, PUT_SONG_ERROR,
    DELETE_SONG, DELETE_SONG_SUCCESS, DELETE_SONG_ERROR,
    RESET_SONG_FOR_EDITING, RESET_SONG_FOR_EDITING_SUCCESS, 
    RESET_MESSAGE_SONG, RESET_MESSAGE_SONG_SUCCESS, 
    CONTINUE_SONG, CONTINUE_SONG_SUCCESS, 
    PLAY_NEW_SONG, PLAY_NEW_SONG_SUCCESS, 
    PAUSE_SONG, PAUSE_SONG_SUCCESS, 
    PLAY_NEXT_SONG, PLAY_NEXT_SONG_SUCCESS, 
    PLAY_PREVIOUS_SONG, PLAY_PREVIOUS_SONG_SUCCESS, 

    GET_ALL_ARTISTS, GET_ALL_ARTISTS_SUCCESS, GET_ALL_ARTISTS_ERROR,
    GET_RANDOM_ARTISTS, GET_RANDOM_ARTISTS_SUCCESS, GET_RANDOM_ARTISTS_ERROR,
    GET_ARTIST_SONGS, GET_ARTIST_SONGS_SUCCESS, GET_ARTIST_SONGS_ERROR,
    GET_ARTIST_EDITING, GET_ARTIST_EDITING_SUCCESS, GET_ARTIST_EDITING_ERROR,
    POST_ARTIST, POST_ARTIST_SUCCESS, POST_ARTIST_ERROR,
    PUT_ARTIST, PUT_ARTIST_SUCCESS, PUT_ARTIST_ERROR,
    DELETE_ARTIST, DELETE_ARTIST_SUCCESS, DELETE_ARTIST_ERROR,
    RESET_ARTIST_FOR_EDITING, RESET_ARTIST_FOR_EDITING_SUCCESS, 

    GET_ALL_PLAYLISTS, GET_ALL_PLAYLISTS_SUCCESS, GET_ALL_PLAYLISTS_ERROR,
    GET_PERSONAL_PLAYLISTS, GET_PERSONAL_PLAYLISTS_SUCCESS, GET_PERSONAL_PLAYLISTS_ERROR,
    GET_PLAYLIST_SONGS, GET_PLAYLIST_SONGS_SUCCESS, GET_PLAYLIST_SONGS_ERROR,
    POST_PLAYLIST, POST_PLAYLIST_SUCCESS, POST_PLAYLIST_ERROR,
    PUT_PLAYLIST, PUT_PLAYLIST_SUCCESS, PUT_PLAYLIST_ERROR,
    DELETE_PLAYLIST, DELETE_PLAYLIST_SUCCESS, DELETE_PLAYLIST_ERROR, 
    
    UPLOAD_IMAGE, UPLOAD_IMAGE_SUCCESS, UPLOAD_IMAGE_ERROR,
    REMOVE_IMAGE, REMOVE_IMAGE_SUCCESS, REMOVE_IMAGE_ERROR,
    UPLOAD_MUSIC, UPLOAD_MUSIC_ERROR, UPLOAD_MUSIC_SUCCESS,
    REMOVE_MUSIC, REMOVE_MUSIC_SUCCESS, REMOVE_MUSIC_ERROR, 
    
    INCREASE_COUNTER,
    EDIT_SEARCH_TEXT, EDIT_SEARCH_TEXT_SUCCESS, EDIT_SEARCH_TEXT_ERROR,
    SEARCH, SEARCH_SUCCESS, SEARCH_ERROR
} from '../actions/songActionTypes';

import { makeErrorMessage, sortArtistsOrPlaylists } from '../../helperFunctions/song';

const initialState = {
    songs: [],
    songForEditing: null,
    playlistSongs: [],
    artists: [],
    artistForEditing: null,
    uploadedImageInfo: null,
    oldImageToDelete: null,
    uploadedMusicInfo: null,
    oldMusicToDelete: null,
    allPlaylists: null,
    playlists: [],
    songPlaying: null, 
    playlistPlayingSongsFrom: null, 
    play: false,
    loading: true,
    message: null,
    error: null
};

export default function songReducer(state = initialState, action) {
    switch(action.type) {
        case GET_ALL_SONGS:
            return { ...state, loading: true };
        case GET_ALL_SONGS_SUCCESS:
            return { ...state, loading: false, songs: action.payload };
        case GET_ALL_SONGS_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get songs", action.payload) };

        case GET_RANDOM_SONGS:
            return { ...state, loading: true };
        case GET_RANDOM_SONGS_SUCCESS:
            return { ...state, loading: false, songs: action.payload, playlistSongs: action.payload };
        case GET_RANDOM_SONGS_ERROR:
            return { ...state, loading: false, ...action.payload };

        case GET_SONG_EDITING:
            return { ...state, loading: true };
        case GET_SONG_EDITING_SUCCESS:
            return { ...state, loading: false, songForEditing: action.payload };
        case GET_SONG_EDITING_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get song", action.payload) };

        case POST_SONG:
            return { ...state, loading: true };
        case POST_SONG_SUCCESS:
            return { ...state, loading: false, uploadedMusicInfo: null, message: { text: "Song successfully created", errorOccurred: false } };
        case POST_SONG_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't make song", action.payload) };
        
        case PUT_SONG:
            return { ...state, loading: true };
        case PUT_SONG_SUCCESS:
            return { ...state, loading: false, uploadedMusicInfo: null, songForEditing: action.payload, message: { text: "Song successfully updated", errorOccurred: false } };
        case PUT_SONG_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't update song", action.payload) };
        
        case DELETE_SONG:
            return { ...state, loading: true };
        case DELETE_SONG_SUCCESS:
            return { ...state, loading: false, songs: state.songs.filter((song => !(song._id === action.payload._id))) };
        case DELETE_SONG_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't delete song", action.payload) };

        case RESET_SONG_FOR_EDITING:
            return { ...state, loading: true };
        case RESET_SONG_FOR_EDITING_SUCCESS:
            return { ...state, loading: false, songForEditing: null };

        case RESET_MESSAGE_SONG:
            return { ...state, loading: true };
        case RESET_MESSAGE_SONG_SUCCESS:
            return { ...state, loading: false, message: null };

        case CONTINUE_SONG:
            return { ...state, loading: true };
        case CONTINUE_SONG_SUCCESS:
            return { ...state, loading: false, play: true };
            
        case PLAY_NEW_SONG:
            return { ...state, loading: true };
        case PLAY_NEW_SONG_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                songPlaying: action.payload, 
                playlistPlayingSongsFrom: state.playlistSongs, 
                play: true 
            };

        case PAUSE_SONG:
            return { ...state, loading: true };
        case PAUSE_SONG_SUCCESS:
            return { ...state, loading: false, play: false };

        case PLAY_NEXT_SONG:
            return { ...state, loading: true };
        case PLAY_NEXT_SONG_SUCCESS:
            let nextSong = state.playlistPlayingSongsFrom[state.playlistPlayingSongsFrom.indexOf(state.songPlaying) + 1] || state.playlistPlayingSongsFrom[0];
            return { 
                ...state, 
                loading: false, 
                play: !(nextSong === state.playlistPlayingSongsFrom[0]),
                songPlaying: nextSong, 
            };

        case PLAY_PREVIOUS_SONG:
            return { ...state, loading: true };
        case PLAY_PREVIOUS_SONG_SUCCESS:
            let previousSong = state.playlistPlayingSongsFrom[state.playlistPlayingSongsFrom.indexOf(state.songPlaying) - 1] || state.playlistPlayingSongsFrom[0];
            return { 
                ...state, 
                loading: false, 
                songPlaying: previousSong, 
                play: true
            };

        /////////////////////////////////////////////////////////////////////////////////////////////

        case GET_ALL_ARTISTS:
            return { ...state, loading: true };
        case GET_ALL_ARTISTS_SUCCESS:
            return { ...state, loading: false, artists: action.payload };
        case GET_ALL_ARTISTS_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get artists", action.payload) };

        case GET_RANDOM_ARTISTS:
            return { ...state, loading: true };
        case GET_RANDOM_ARTISTS_SUCCESS:
            return { ...state, loading: false, artists: action.payload };
        case GET_RANDOM_ARTISTS_ERROR:
            return { ...state, loading: false, ...action.payload };

        case GET_ARTIST_SONGS:
            return { ...state, loading: true };
        case GET_ARTIST_SONGS_SUCCESS:
            return { ...state, loading: false, songs: action.payload, playlistSongs: action.payload };
        case GET_ARTIST_SONGS_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get the artist's songs", action.payload) };

        case GET_ARTIST_EDITING:
            return { ...state, loading: true };
        case GET_ARTIST_EDITING_SUCCESS:
            return { ...state, loading: false, artistForEditing: action.payload };
        case GET_ARTIST_EDITING_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get artist", action.payload) };

        case POST_ARTIST:
            return { ...state, loading: true };
        case POST_ARTIST_SUCCESS:
            return { ...state, loading: false, uploadedImageInfo: null, message: { text: "Artist successfully created", errorOccurred: false } };
        case POST_ARTIST_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't make artist", action.payload) };

        case PUT_ARTIST:
            return { ...state, loading: true };
        case PUT_ARTIST_SUCCESS:
            return { ...state, loading: false, uploadedImageInfo: null, artistForEditing: action.payload, message: { text: "Artist successfully updated", errorOccurred: false } };
        case PUT_ARTIST_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't update artist", action.payload) };

        case DELETE_ARTIST:
            return { ...state, loading: true };
        case DELETE_ARTIST_SUCCESS:
            let deletedArtists = state.artists.filter(artist => !(artist._id === action.payload._id));
            return { ...state, loading: false, artists: deletedArtists.sort(sortArtistsOrPlaylists) };
        case DELETE_ARTIST_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't delete artist", action.payload) };

        case RESET_ARTIST_FOR_EDITING:
            return { ...state, loading: true };
        case RESET_ARTIST_FOR_EDITING_SUCCESS:
            return { ...state, loading: false, artistForEditing: null };

        /////////////////////////////////////////////////////////////////////////////////////////////
        
        case GET_ALL_PLAYLISTS:
            return { ...state, loading: true };
        case GET_ALL_PLAYLISTS_SUCCESS:
            return { ...state, loading: false, allPlaylists: action.payload };
        case GET_ALL_PLAYLISTS_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get the playlists", action.payload) };
        
        case GET_PERSONAL_PLAYLISTS:
            return { ...state, loading: true };
        case GET_PERSONAL_PLAYLISTS_SUCCESS:
            return { ...state, loading: false, playlists: action.payload };
        case GET_PERSONAL_PLAYLISTS_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get your playlists", action.payload) };

        case GET_PLAYLIST_SONGS:
            return { ...state, loading: true };
        case GET_PLAYLIST_SONGS_SUCCESS:
            let thePlaylistSongs = [...action.payload];
            return { ...state, loading: false, playlistSongs: thePlaylistSongs };
        case GET_PLAYLIST_SONGS_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't get playlist", action.payload) };
        
        case POST_PLAYLIST:
            return { ...state, loading: true };
        case POST_PLAYLIST_SUCCESS:
            let newPlaylists = state.playlists.slice();
            newPlaylists.push(action.payload);
            return { ...state, loading: false, playlists: newPlaylists.sort(sortArtistsOrPlaylists) };
        case POST_PLAYLIST_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't make the playlist", action.payload) };
        
        case PUT_PLAYLIST:
            return { ...state, loading: true };
        case PUT_PLAYLIST_SUCCESS:
            let editedPlaylists = state.playlists.filter(playlist => !(playlist._id === action.payload.playlistFromBack._id));
            editedPlaylists.push(action.payload.playlistFromBack);
            return { ...state, loading: false, playlists: editedPlaylists.sort(sortArtistsOrPlaylists), playlistSongs: action.payload.playlistFromBack.songs };
        case PUT_PLAYLIST_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't update the playlist", action.payload) };
        
        case DELETE_PLAYLIST:
            return { ...state, loading: true };
        case DELETE_PLAYLIST_SUCCESS:
            let deletedPlaylists = state.playlists.filter(playlist => !(playlist._id === action.payload._id));
            return { ...state, loading: false, playlists: deletedPlaylists.sort(sortArtistsOrPlaylists) };
        case DELETE_PLAYLIST_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't delete the playlist", action.payload) };

        /////////////////////////////////////////////////////////////////////////////////////////////
        
        case UPLOAD_IMAGE:
            return { ...state, loading: true };
        case UPLOAD_IMAGE_SUCCESS:
            const oldImageToDelete = state.uploadedImageInfo;
            return { ...state, loading: false, uploadedImageInfo: action.payload, oldImageToDelete };
        case UPLOAD_IMAGE_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't upload the image", action.payload) };
        
        case REMOVE_IMAGE:
            return { ...state, loading: true };
        case REMOVE_IMAGE_SUCCESS:
            if (action.payload === state.uploadedImageInfo.publicId) {
                return { ...state, loading: false, uploadedImageInfo: null };
            }
            else if (action.payload === state.oldImageToDelete.publicId) {
                return { ...state, loading: false, oldImageToDelete: null };
            }
            else {
                return { ...state, loading: false }
            }
        case REMOVE_IMAGE_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't remove the image", action.payload) };
        
        case UPLOAD_MUSIC:
            return { ...state, loading: true };
        case UPLOAD_MUSIC_SUCCESS:
            const oldMusicToDelete = state.uploadedMusicInfo;
            return { ...state, loading: false, uploadedMusicInfo: action.payload, oldMusicToDelete };
        case UPLOAD_MUSIC_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't upload the music", action.payload) };
        
        case REMOVE_MUSIC:
            return { ...state, loading: true };
        case REMOVE_MUSIC_SUCCESS:
            if (action.payload === state.uploadedMusicInfo.publicId) {
                return { ...state, loading: false, uploadedMusicInfo: null }
            }
            else if (action.payload === state.oldMusicToDelete.publicId) {
                return { ...state, loading: false, oldMusicToDelete: null }
            }
            else {
                return { ...state, loading: false }
            }
        case REMOVE_MUSIC_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't remove the music", action.payload) };

        /////////////////////////////////////////////////////////////////////////////////////////////

        case INCREASE_COUNTER:
            return { ...state };

        case EDIT_SEARCH_TEXT:
            return { ...state, loading: true };
        case EDIT_SEARCH_TEXT_SUCCESS:
            return { ...state, loading: false, searchText: action.payload };
        case EDIT_SEARCH_TEXT_ERROR:
            return { ...state, loading: false, ...action.payload };

        case SEARCH:
            return { ...state, loading: true, songs: [], artists: [], playlistPlayingSongsFrom: [] };
        case SEARCH_SUCCESS:
            return { ...state, loading: false, songs: action.payload.songs, artists: action.payload.artists, playlistSongs: action.payload.songs, playlistPlayingSongsFrom: action.payload.songs };
        case SEARCH_ERROR:
            return { ...state, loading: false, message: makeErrorMessage("Couldn't search", action.payload) };

        default:
            return { ...state };
    };
};

