import {
    REGISTER_USER, REGISTER_USER_SUCCESS, REGISTER_USER_ERROR,
    LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_ERROR,
    LOGOUT_USER, LOGOUT_USER_SUCCESS, LOGOUT_USER_ERROR,
    CHECK_IF_AUTHENTICATED_USER, CHECK_IF_AUTHENTICATED_USER_SUCCESS, CHECK_IF_AUTHENTICATED_USER_ERROR,
    GET_CURRENT_USER, GET_CURRENT_USER_SUCCESS, GET_CURRENT_USER_ERROR,
    CLEAR_EDIT_USER_INFO, CLEAR_EDIT_USER_INFO_SUCCESS, CLEAR_EDIT_USER_INFO_ERROR,
    CLEAR_USER_MESSAGE, CLEAR_USER_MESSAGE_SUCCESS, CLEAR_USER_MESSAGE_ERROR,
    PUT_USER, PUT_USER_SUCCESS, PUT_USER_ERROR,
    PUT_PASSWORD, PUT_PASSWORD_SUCCESS, PUT_PASSWORD_ERROR,
    POST_ADMIN, POST_ADMIN_SUCCESS, POST_ADMIN_ERROR
} from '../actions/authActionTypes';

const initialState = {
    user: {userName: '', role: ''},
    loading: true,
    authenticated: false,
    error: null
};

export default function authReducer(state = initialState, action) {
    switch (action.type) {
        case REGISTER_USER:
            return {...state, loading: true};
        case REGISTER_USER_SUCCESS:
            return {...state, ...action.payload, loading: false};
        case REGISTER_USER_ERROR:
            return {...state, ...action.payload, loading: false};
        
        case LOGIN_USER:
            return {...state, loading: true};
        case LOGIN_USER_SUCCESS:
            return {...state, ...action.payload, loading: false};
        case LOGIN_USER_ERROR:
            return {...state, ...action.payload, loading: false};
        
        case LOGOUT_USER:
            return {...state, loading: true};
        case LOGOUT_USER_SUCCESS:
            return {...state, ...action.payload, loading: false};
        case LOGOUT_USER_ERROR:
            return {...state, ...action.payload, loading: false};

        case CHECK_IF_AUTHENTICATED_USER:
            return {...state, loading: true};
        case CHECK_IF_AUTHENTICATED_USER_SUCCESS:
            return {...state, ...action.payload, loading: false};
        case CHECK_IF_AUTHENTICATED_USER_ERROR:
            return { ...initialState, loading: false };

        case GET_CURRENT_USER:
            return {...state, loading: true};
        case GET_CURRENT_USER_SUCCESS:
            return {...state, currentUserToEdit: {...action.payload}, loading: false};
        case GET_CURRENT_USER_ERROR:
            return {...state, ...action.payload, loading: false};
        
        case CLEAR_EDIT_USER_INFO:
            return {...state, loading: true};
        case CLEAR_EDIT_USER_INFO_SUCCESS:
            return {...state, currentUserToEdit: null, loading: false};
        case CLEAR_EDIT_USER_INFO_ERROR:
            return {...state, ...action.payload, loading: false};

        case CLEAR_USER_MESSAGE:
            return {...state, loading: true};
        case CLEAR_USER_MESSAGE_SUCCESS:
            return {...state, message: null , loading: false};
        case CLEAR_USER_MESSAGE_ERROR:
            return {...state, ...action.payload, loading: false};

        case PUT_USER:
            return {...state, loading: true};
        case PUT_USER_SUCCESS:
            return {...state, ...action.payload, loading: false};
        case PUT_USER_ERROR:
            return {...state, ...action.payload, loading: false};
        
        case PUT_PASSWORD:
            return {...state, loading: true};
        case PUT_PASSWORD_SUCCESS:
            return {...state, ...action.payload, loading: false};
        case PUT_PASSWORD_ERROR:
            return {...state, ...action.payload, loading: false};

        case POST_ADMIN:
            return { ...state, loading: true };
        case POST_ADMIN_SUCCESS:
            return { ...state, ...action.payload, loading: false };
        case POST_ADMIN_ERROR:
            return { ...state, loading: false, ...action.payload };

        default:
            return { ...state };
    };
};