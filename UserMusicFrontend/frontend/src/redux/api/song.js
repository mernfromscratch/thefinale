import axios from 'axios';

export const getAllSongs = async () => {
    const songs = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/songs/all`);
    return songs.data;
};

export const getRandomSongs = async (amountToGet) => {
    const songs = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/songs/random/${amountToGet}`, {withCredentials: true});
    return songs.data;
};

export const getSong = async (_id) => {
    const songFromBack = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/song/${_id}`, {withCredentials: true});
    return songFromBack.data;
};

export const postSong = async (song) => {
    const songFromBack = await axios.post(`${process.env.REACT_APP_BACKENDURL}/api/song/`, song, {withCredentials: true});
    return songFromBack.data;
};

export const putSong = async (song) => {
    const songFromBack = await axios.put(`${process.env.REACT_APP_BACKENDURL}/api/song/${song._id}`, song, {withCredentials: true});
    return songFromBack.data;
};

export const deleteSong = async (_id) => {
    const songFromBack = await axios.delete(`${process.env.REACT_APP_BACKENDURL}/api/song/${_id}`, {withCredentials: true});
    return songFromBack.data;
};

///////////////////////////////////////////////////////////////////////

export const getAllArtists = async () => {
    const artists = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/artists/all`, {withCredentials: true});
    return artists.data;
};

export const getRandomArtists = async (amountToGet) => {
    const artists = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/artists/random/${amountToGet}`, {withCredentials: true});
    return artists.data;
};

export const getArtistSongs = async (_id) => {
    const songsFromBack = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/artist/${_id}/allSongs`, {withCredentials: true});
    return songsFromBack.data;
};

export const getArtist = async (_id) => {
    const artistFromBack = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/artist/${_id}`, {withCredentials: true});
    return artistFromBack.data;
};

export const postArtist = async (artist) => {
    const artistFromBack = await axios.post(`${process.env.REACT_APP_BACKENDURL}/api/artist/`, artist, {withCredentials: true});
    return artistFromBack.data;
};

export const putArtist = async (artist) => {
    const artistFromBack = await axios.put(`${process.env.REACT_APP_BACKENDURL}/api/artist/${artist._id}`, artist, {withCredentials: true});
    return artistFromBack.data;
};

export const deleteArtist = async (_id) => {
    const artistFromBack = await axios.delete(`${process.env.REACT_APP_BACKENDURL}/api/artist/${_id}`, {withCredentials: true});
    return artistFromBack.data;
};

///////////////////////////////////////////////////////////////////////

export const getAllPlaylists = async () => {
    const playlists = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/playlists/all`, {withCredentials: true});
    return playlists.data;
};

export const getPersonalPlaylists = async () => {
    const playlists = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/playlists/personal`, {withCredentials: true});
    return playlists.data;
};

export const getPlaylistSongs = async (_id) => {
    const playlistSongs = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/playlist/${_id}/songs`, {withCredentials: true});
    return playlistSongs.data;
};

export const postPlaylist = async (playlist) => {
    const playlistFromBack = await axios.post(`${process.env.REACT_APP_BACKENDURL}/api/playlist/`, playlist, {withCredentials: true});
    return playlistFromBack.data;
};

export const putPlaylist = async (playlist, song, displaying) => {
    const playlistFromBack = await axios.put(`${process.env.REACT_APP_BACKENDURL}/api/playlist/${playlist._id}`, {playlist, song, displaying}, {withCredentials: true});
    return playlistFromBack.data;
};

export const deletePlaylist = async (_id) => {
    const playlistFromBack = await axios.delete(`${process.env.REACT_APP_BACKENDURL}/api/playlist/${_id}`, {withCredentials: true});
    return playlistFromBack.data;
};

///////////////////////////////////////////////////////////////////////

export const uploadImage = async (image) => {
    const pictureInfo = await axios.post(`${process.env.REACT_APP_BACKENDURL}/api/uploadimage`, {image}, {withCredentials: true});
    return pictureInfo.data;
};

export const removeImage = async (imageId) => {
    const response = await axios.delete(`${process.env.REACT_APP_BACKENDURL}/api/removeimage/${imageId}`, {withCredentials: true});
    return response.data;
};

export const uploadMusic = async (music) => {
    const musicInfo = await axios.post(`${process.env.REACT_APP_BACKENDURL}/api/uploadmusic`, {music}, {withCredentials: true});
    return musicInfo.data;
};

export const removeMusic = async (musicId) => {
    const response = await axios.delete(`${process.env.REACT_APP_BACKENDURL}/api/removemusic/${musicId}`, {withCredentials: true});
    return response.data;
};

///////////////////////////////////////////////////////////////////////

export const increaseCounter = async (_id) => {
    const response = await axios.put(`${process.env.REACT_APP_BACKENDURL}/api/song/increasecounter/${_id}`, {}, {withCredentials: true});
    return response.data;
}

export const search = async (textToSearchFor) => {
    const response = await axios.get(`${process.env.REACT_APP_BACKENDURL}/api/search/${textToSearchFor}`, {withCredentials: true});
    return response.data;
};