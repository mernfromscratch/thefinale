const axios = require('axios');

export const register = async (user) => {
    const userRegistered = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/register`, user, {withCredentials: true});
    return userRegistered.data;        
};

export const login = async (userName, password) => {
    const userLogin = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/login`, {userName, password}, {withCredentials: true});        
    return userLogin.data;
};

export const logout = async () => {
    const userLogout = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/logout`, {}, {withCredentials: true});
    return userLogout.data;
};

export const isAuthenticated = async () => {
    const isAuthenticated = await axios.get(`${process.env.REACT_APP_BACKENDURL}/user/authenticated`, {withCredentials: true});
    return isAuthenticated.data;
};

export const getCurrentUser = async () => {
    const currentUser = await axios.get(`${process.env.REACT_APP_BACKENDURL}/user/getCurrent`, {withCredentials: true});
    return currentUser.data;
};

export const putUser = async (user) => {
    const userPut = await axios.put(`${process.env.REACT_APP_BACKENDURL}/user/edit`, user, {withCredentials: true});
    return userPut.data;        
};

export const putPassword = async (passwordOldAndNew) => {
    const passwordPut = await axios.put(`${process.env.REACT_APP_BACKENDURL}/user/changePassword`, passwordOldAndNew, {withCredentials: true});
    return passwordPut.data;        
};

export const postAdmin = async (newAdmin, password) => {
    const response = await axios.post(`${process.env.REACT_APP_BACKENDURL}/user/makeAdmin`, {newAdmin, password}, {withCredentials: true});
    return response.data;
};
