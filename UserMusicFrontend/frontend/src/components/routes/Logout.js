import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logoutUserAction } from '../../redux/actions/authActions';

const Logout = ({history}) => {

    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    useEffect(() => {
        dispatch(logoutUserAction());
    }, []);

    useEffect(() => {
        history.push("/");
    }, [authState.authenticated]);

    return (
        <div>
            Logging out
        </div>
    )
};

export default Logout;