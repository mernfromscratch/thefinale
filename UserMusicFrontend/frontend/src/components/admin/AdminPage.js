import Button from '@material-ui/core/Button';
import MusicNoteRounded from '@material-ui/icons/MusicNoteRounded';
import PersonAddRounded from '@material-ui/icons/PersonAddRounded';
import EditRounded from '@material-ui/icons/EditRounded';

const AdminPage = ({ history }) => {

    const adminButtonStyle = { backgroundColor: "#FFF", color: "#000" };

    return (
        <div className="container h-100 hideOverflowX">
            <div className="row h-50 align-items-center">
                <div className="offset-4 col-4">
                    <div className="row mt-4 justify-content-center">
                        <Button variant="contained" size="large" style={adminButtonStyle} onClick={() => history.push("/create/artist")}>
                            <PersonAddRounded />
                            <div className="pl-2">
                                Add artist
                            </div>
                        </Button>
                    </div>
                    <div className="row mt-4">
                        <Button variant="contained" size="large" style={adminButtonStyle} onClick={() => history.push("/create/song")}>
                            <MusicNoteRounded />
                            <div className="pl-1">
                                Add a song
                            </div>
                        </Button>
                    </div>
                    <div className="row mt-4">
                        <Button variant="contained" size="large" style={adminButtonStyle} onClick={() => history.push("/edit")}>
                            <EditRounded />
                            <div className="pl-1">
                                Edit an item
                            </div>
                        </Button>
                    </div>
                    <div className="row mt-4">
                        <Button variant="contained" size="large" style={adminButtonStyle} onClick={() => history.push("/admin/create")}>
                            <PersonAddRounded />
                            <div className="pl-2">
                                Make new admin
                            </div>
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AdminPage;