import { useState, anchorRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { editSearchTextAction } from '../redux/actions/songActions';
import { logoutUserAction } from '../redux/actions/authActions';

import './NavBar.css';

import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';

import HomeRounded from '@material-ui/icons/HomeRounded';
import AccountCircle from '@material-ui/icons/AccountCircle';
import AccountBoxRounded from '@material-ui/icons/AccountBoxRounded';
import SearchIcon from '@material-ui/icons/Search';

const NavBar = () => {

    const history = useHistory();
    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);
    const [anchorEl, setAnchorEl] = useState(null);


    const logout = () => {
        dispatch(logoutUserAction());
        setAnchorEl(null);
        history.push("/");
    };

    const gotoProfile = () => {
        setAnchorEl(null);
        history.push("/user/edit");
    }

    const gotoPasswordChange = () => {
        setAnchorEl(null);
        history.push("/user/passwordChange");
    }

    const handleToggle = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (event) => {
        setAnchorEl(null);
    };



    return (
        <header className="d-flex flex-row themeColorsDarker">
            <div className="row flex-grow-1 g-0">
                <div className="col-6">
                    <IconButton color="inherit" edge="end" className="pl-2" onClick={() => history.push('/')}>
                        <HomeRounded fontSize="large" />
                    </IconButton>
                    {authState.admin
                        ?   (
                                <IconButton color="inherit" edge="end" className="pl-4" onClick={() => history.push('/admin')}>
                                    <AccountBoxRounded fontSize="large" />
                                </IconButton>
                            )
                        :   (
                            <></>
                        )

                    }
                </div>
                <div className="col-6">
                    <div className="row flex-grow-1 g-0">
                        {authState.authenticated 
                            &&  (   
                                    <div className="offset-6 col-4">
                                        <Paper component="div" className="mt-1 mb-1 pl-4 d-flex align-items-center float-right">
                                            <InputBase
                                                className="flex-grow-1"
                                                placeholder="Search"
                                                onChange={(e) => dispatch(editSearchTextAction(e.target.value))}
                                            />
                                            <IconButton className="p-6" onClick={() => history.push("/search")}>
                                                <SearchIcon />
                                            </IconButton>
                                        </Paper>
                                    </div>
                            )
                        }
                        {authState.authenticated 
                            ?   (   
                                    <div className="col-2">
                                        <IconButton color="inherit" edge="end" className="mr-4">
                                            <AccountCircle fontSize="large" ref={anchorRef} onClick={handleToggle} />
                                            <Menu anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                                                <MenuItem onClick={() => gotoProfile()}>My account</MenuItem>
                                                <MenuItem onClick={() => gotoPasswordChange()}>Change password</MenuItem>
                                                <MenuItem onClick={() => logout()}>Logout</MenuItem>
                                            </Menu>
                                        </IconButton>
                                    </div>
                                )
                            :   (   
                                    <div className="offset-8 col-2">
                                        <Button color="inherit" className="mt-3" onClick={() => history.push('/user/login')}> 
                                            Login 
                                        </Button>
                                    </div>
                                )
                        }
                    </div>
                </div>
            </div>
        </header>
    );
};

export default NavBar;