import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import "./Playlists.css";

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import AddBoxSharp from '@material-ui/icons/AddBoxSharp';
import { postPlaylistAction } from '../../redux/actions/songActions';

const Playlists = () => {

    const [value, setValue] = useState(0);
    const [newPlaylistVisible, setNewPlaylistVisible] = useState(false);
    const [newPlaylistName, setNewPlaylistName] = useState("");
    const dispatch = useDispatch();
    const history = useHistory();
    const { songState } = useSelector(state => state);

    const playlistChange = (event, newValue) => {
        setValue(newValue);
    };

    const newPlaylistChange = () => {
        setNewPlaylistVisible(!newPlaylistVisible);
        setNewPlaylistName("");
    };

    const newPlaylistBlur = () => {
        setNewPlaylistVisible(false);
        !(newPlaylistName === "") && dispatch(postPlaylistAction({name: newPlaylistName}));
    };

    return(
        <div className="container-fluid mh-100">
                <div className="playlistsItem font-weight-bold row" onClick={newPlaylistChange}>
                    <AddBoxSharp className="mr-2" />
                    New playlist
                </div>
                {   newPlaylistVisible && <div className="playlistsItem row mt-2">
                        <TextField style={{ textTransform: "none" }} size="small" variant="standard" margin="none" className="themeColorsReverse" value={newPlaylistName} onBlur={newPlaylistBlur} inputRef={input => input && input.focus()} onChange={(e) => setNewPlaylistName(e.target.value)} />
                    </div>
                }
                <Tabs
                    orientation="vertical"
                    value={value}
                    onChange={playlistChange}
                    className="mt-4"
                >
                    {songState.playlists.map( (playlistToTab) => (<Tab key={playlistToTab["name"]} label={playlistToTab["name"]} onClick={() => history.push(`/playlist/${playlistToTab._id}`)} />) )}
                </Tabs>
        </div>
    )
};

export default Playlists;