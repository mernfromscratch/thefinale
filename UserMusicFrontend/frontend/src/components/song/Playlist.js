import { useEffect, useState } from "react";
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { deletePlaylistAction, getPlaylistSongsAction, putPlaylistAction } from "../../redux/actions/songActions";
import { getSongAttributes } from "../../helperFunctions/song";

import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';

import ItemTable from '../cards/ItemTable';

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      height: '100%',
    },
  }));

const Playlist = ({ history }) => {

    const classes = useStyles();
    const [playlistViewing, setPlaylistViewing] = useState({});
    const [playlistEditing, setPlaylistEditing] = useState({});
    const [songAttributes, setSongAttributes] = useState([]);
    const { songState } = useSelector(state => state);
    const { _id } = useParams();
    const dispatch = useDispatch();

    useEffect(() => {
        playlistViewing._id && setPlaylistEditing(playlistViewing);
    }, [playlistViewing]);

    useEffect(() => {
        // If playlist was deleted, go to main page
        const playlistIds = songState.playlists.map((playlistToMap) => playlistToMap._id);
        !playlistIds.includes(_id) && history.push("/");

        // If playlist details were updated
        playlistIds.includes(_id) && setPlaylistViewing(songState.playlists.find((playlist) => (playlist._id === _id)));
    }, [songState.playlists]);

    useEffect(() => {
        dispatch(getPlaylistSongsAction(_id));
        setPlaylistViewing(songState.playlists.find((playlist) => (playlist._id === _id)));
        getSongAttributes()
            .then((attributes) => setSongAttributes(attributes));
    }, [_id]);

    const isOwnerOfPlaylist = () => {
        const playlistIds = songState.playlists.map(playlist => playlist._id);
        return playlistIds.includes(_id);
    };

    const putPlaylistSongs = (song, addOrRemove) => {
        let currentPlaylist = songState.playlists.find((playlist) => playlist._id === _id);
        let playlistSongsPut = [];
        if (addOrRemove === "add") { playlistSongsPut = [...songState.playlistSongs, song] }
        else if (addOrRemove === "remove") { playlistSongsPut = songState.playlistSongs.filter((songInPlaylist) => !(songInPlaylist._id === song._id)); };
        currentPlaylist.songs = playlistSongsPut;
        dispatch(putPlaylistAction(currentPlaylist, null, true));
    };

    const putPlaylist = () => {
        dispatch(putPlaylistAction(playlistEditing, null, true));
    };

    const deletePlaylist = () => {
        dispatch(deletePlaylistAction(_id))
    };

    const playlistTop = () => (
        <>
            <div className="row">
                <div className="col-6 px-0">
                    <CardMedia
                        className={classes.root}
                        image="https://source.unsplash.com/random"
                        title="Artist image"
                    />
                </div>
                <div className="col-6 pr-0 display-3 d-flex">
                    { playlistViewing && playlistViewing.name }
                </div>
            </div>
        </>
    );

    return (
        <div>
            <div className="container overflow-hidden">
                {playlistTop()}
            </div>
            <div className="hideOverflowX">
                { !songState.loading && (songState.playlistSongs.length > 0) && <ItemTable 
                                                                                    items={songState.playlistSongs} 
                                                                                    itemAttributes={songAttributes} 
                                                                                    itemType={"song"} 
                                                                                    putPlaylistSongs={putPlaylistSongs}
                                                                                    playlistRelated={{playlistEditing, setPlaylistEditing, putPlaylist, deletePlaylist, isOwnerOfPlaylist, playlistViewing}}
                                                                                    inPlaylist={true}
                                                                                />}
            </div>
        </div>
    );
};

export default Playlist;