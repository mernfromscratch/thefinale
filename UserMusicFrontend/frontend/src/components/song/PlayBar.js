import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect, useRef } from "react";
import { makeTimeHumanFriendly } from "../../helperFunctions/song";
import { continueSongAction, pauseSongAction, playPreviousSongAction, playNextSongAction, increaseCounterAction } from "../../redux/actions/songActions";

import IconButton from '@material-ui/core/IconButton';
import Slider from '@material-ui/core/Slider';

import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import VolumeDownIcon from '@material-ui/icons/VolumeDown';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import VolumeMuteIcon from '@material-ui/icons/VolumeMute';

const PlayBar = () => {

    const dispatch = useDispatch();
    const { songState } = useSelector(state => state);
    const audioComponent = useRef(null);
    const [ songProgress, setSongProgress ] = useState(0);
    const [ secondsOfSongPlayed, setSecondsOfSongPlayed ] = useState(0);
    const [ lengthOfSongSeconds, setLengthOfSongSeconds ] = useState(null);
    const [ volume, setVolume ] = useState(1);

    useEffect(() => {
        songState.play && audioComponent.current.play();
        songState.songPlaying && !songState.play && audioComponent.current.pause();
    }, [songState.play]);

    const songEnded = (e) => {
        dispatch(increaseCounterAction(songState.songPlaying._id));
        dispatch(playNextSongAction());
    };

    const changeSongProgress = (event, newValue) => {
        audioComponent.current.currentTime = newValue;
        setSecondsOfSongPlayed(newValue);
    };

    const changeVolume = (event, newValue) => {
        setVolume(newValue);
        audioComponent.current.volume = newValue;
    };

    const updateAmountOfSongPlayed = (e) => {
        setSecondsOfSongPlayed(audioComponent.current.currentTime);
        setSongProgress(Math.floor(audioComponent.current.currentTime));
    };

    const volumeIcon = () => {
        if (volume >= 0.5) {return (
            <IconButton>
                <VolumeUpIcon />
            </IconButton>
        )} 
        else if (volume < 0.5 && !(volume === 0)) {return (
            <IconButton>
                <VolumeDownIcon />
            </IconButton>
        )}
        else {return (
            <IconButton>
                <VolumeMuteIcon />
            </IconButton>
        )}
    };

    return (
        <div className="container-fluid overflow-hidden">
            <div className="row d-flex justify-content-between">
                <div className="col-4 d-flex justify-content-center align-items-center">
                    {songState.songPlaying && songState.songPlaying.title}
                </div>
                <div className="col-4 d-flex justify-content-center">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 d-flex justify-content-center">
                                <IconButton onClick={() => songState.songPlaying && dispatch(playPreviousSongAction())}>
                                    <SkipPreviousIcon />
                                </IconButton>
                                <IconButton onClick={() => !songState.songPlaying ? {} : (songState.play ? dispatch(pauseSongAction()) : dispatch(continueSongAction()))}>
                                    { songState.play
                                    ? <PauseIcon />
                                    : <PlayArrowIcon />
                                    }
                                </IconButton>
                                <IconButton onClick={() => songState.songPlaying && dispatch(playNextSongAction())}>
                                    <SkipNextIcon />
                                </IconButton>
                            </div>
                            <audio 
                                ref={audioComponent}
                                style={{ display: "none" }}
                                autoPlay
                                src={songState.songPlaying && songState.songPlaying.songUrl}
                                onDurationChangeCapture={(e) => setLengthOfSongSeconds(Math.floor(e.target.duration))} 
                                onTimeUpdate={updateAmountOfSongPlayed} 
                                onEndedCapture={songEnded}
                            />
                        </div>
                        <div className="row">
                            <div className="col-1 g-0">
                                { songState.songPlaying && makeTimeHumanFriendly(songProgress) }
                            </div>
                            <div className="col-10">
                                <Slider variant="determinate" min={0} max={lengthOfSongSeconds} step={1} value={secondsOfSongPlayed} onChange={changeSongProgress} />
                            </div>
                            <div className="col-1 g-0">
                                { songState.songPlaying && makeTimeHumanFriendly(lengthOfSongSeconds) }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-4 d-flex justify-content-center align-items-center">
                    <div className="container-fluid row align-items-center d-none d-md-flex">
                        <div className="offset-md-6 col-md-2">
                            { volumeIcon() }
                        </div>
                        <div className="col-md-4 pt-2">
                            <Slider variant="determinate" min={0} max={1} step={0.01} value={volume} onChange={changeVolume} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PlayBar;