import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { putPlaylistAction, searchAction } from "../../redux/actions/songActions";
import { getSongAttributes } from "../../helperFunctions/song";

import ItemTable from "../cards/ItemTable";
import ItemCards from "../cards/ItemCards";

const SearchPage = () => {

    const dispatch = useDispatch();
    const { songState } = useSelector(state => state);
    const [ attributes, setAttributes ] = useState([]);

    useEffect(() => {
        dispatch(searchAction(songState.searchText));
        getSongAttributes()
            .then(attributes => { setAttributes(attributes) });
    }, []);

    useEffect(() => {
        dispatch(searchAction(songState.searchText));
    }, [songState.searchText]);

    const addPlaylistSongs = (song, addOrRemove, chosenPlaylist) => {
        let playlistToAddTo = songState.playlists.find((playlist) => playlist._id === chosenPlaylist._id);
        dispatch(putPlaylistAction(playlistToAddTo, song, false));
    };

    return (
        <>
            { songState.songs && songState.artists &&
                (
                    <>
                        <div>
                            <ItemCards items={songState.artists} />
                        </div>
                        <div>
                            <ItemTable items={songState.songs} itemAttributes={attributes} itemType={"song"} putPlaylistSongs={addPlaylistSongs} />
                        </div>
                    </>
                )
            }
        </>
    )
};

export default SearchPage;