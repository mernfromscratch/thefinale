import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getArtistSongsAction, putPlaylistAction, deleteSongAction } from '../../redux/actions/songActions';
import { getArtist, getSongAttributes } from "../../helperFunctions/song";

import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';


import ItemTable from "../cards/ItemTable";

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      height: '100%',
    },
  }));

const ArtistPage = ({ history }) => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const { songState } = useSelector(state => state);
    const { _id } = useParams();
    const [ artist, setArtist ] = useState({});
    const [attributes, setAttributes] = useState([]);

    useEffect(() => {
        dispatch(getArtistSongsAction(_id));
        getSongAttributes()
            .then(attributes => { setAttributes(attributes) });
        getArtist(_id)
            .then(artist => { setArtist(artist) });
    }, []);

    const addPlaylistSongs = (song, addOrRemove, chosenPlaylist) => {
        let playlistToAddTo = songState.playlists.find((playlist) => playlist._id === chosenPlaylist._id);
        dispatch(putPlaylistAction(playlistToAddTo, song, false));
    };

    const deleteSong = (_idOfSong) => {
        dispatch(deleteSongAction(_idOfSong));
    };

    const artistTop = () => (
        <>
            <div className="row m-auto">
                <div className="col-6 px-0">
                    <CardMedia
                        className={classes.root}
                        image={artist["pictureUrl"] || "https://source.unsplash.com/random"}
                        title="Artist image"
                    />
                </div>
                <div className="col-6 pr-0 display-1 d-flex">
                    { artist && artist.name }
                </div>
            </div>
        </>
    );

    return (
        <div>
            <div className="container-fluid">
                {artistTop()}
            </div>
            <div>
                <ItemTable 
                    items={songState.songs} 
                    itemAttributes={attributes} 
                    itemType={"song"} 
                    putPlaylistSongs={addPlaylistSongs} 
                    deleteItem={deleteSong} 
                />
            </div>
        </div>
    );
};

export default ArtistPage;