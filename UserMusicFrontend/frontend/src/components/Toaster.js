import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { clearUserMessageAction } from '../redux/actions/authActions';
import { resetSongMessageAction } from '../redux/actions/songActions';

import Snackbar from '@material-ui/core/Snackbar';

const Toaster = () => {

    const [ open, setOpen ] = useState(false);
    const { songState, authState } = useSelector(state => state);
    const dispatch = useDispatch();

    useEffect(() => {
        (songState.message) && (songState.message.text) && setOpen(true);
        (authState.message) && (authState.message.text) && setOpen(true);
    }, [songState.message, authState.message])

    const handleClose = () => {
        setOpen(false);
        (songState.message) && (songState.message.text) && dispatch(resetSongMessageAction());
        (authState.message) && (authState.message.text) && dispatch(clearUserMessageAction());
    };

    return (
        <Snackbar
            autoHideDuration={2000}
            message={songState.message ? songState.message.text : (authState.message ? authState.message.text : "" )}
            onClose={handleClose}
            open={open}
        />
    );
};

export default Toaster;