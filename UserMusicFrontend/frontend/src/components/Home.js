import { useSelector, useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import { deleteSongAction, getRandomArtistsAction, getRandomSongsAction, putPlaylistAction } from '../redux/actions/songActions';
import ItemCards from './cards/ItemCards';
import ItemTable from './cards/ItemTable';

import registerImage from "../images/listening-music-headphone.jpg";
import logoImage from "../images/Spottyfly_Transparent_cropped.png";
import { useHistory } from 'react-router-dom';
import { getSongAttributes } from '../helperFunctions/song';

const Home = ({ showRegister, setShowRegister}) => {

    const { songState, authState } = useSelector(state => state);
    const [attributes, setAttributes] = useState([]);
    const registerBackStyle = { backgroundImage: `url(${registerImage})`, backgroundSize: "cover", backgroundRepeat: "no-repeat" };
    const registerLogoStyle = { backgroundImage: `url(${logoImage})`, backgroundSize: "contain", backgroundPosition: "center", backgroundRepeat: "no-repeat" };
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        return () => {
            setShowRegister(false);
        };
    }, []);

    useEffect(() => {
        authState && !authState.authenticated && setShowRegister(true);
        authState && authState.authenticated && setShowRegister(false);
    });

    useEffect(() => {
        authState.authenticated && dispatch(getRandomArtistsAction(process.env.REACT_APP_AMOUNT_OF_RANDOM_ARTISTS));
        authState.authenticated && getSongAttributes().then(attributes => { setAttributes(attributes) });
        authState.authenticated && dispatch(getRandomSongsAction(process.env.REACT_APP_AMOUNT_OF_RANDOM_SONGS));
    }, [authState.authenticated]);

    const addPlaylistSongs = (song, addOrRemove, chosenPlaylist) => {
        let playlistToAddTo = songState.playlists.find((playlist) => playlist._id === chosenPlaylist._id);
        dispatch(putPlaylistAction(playlistToAddTo, song, false));
    };

    const deleteSong = (_idOfSong) => {
        dispatch(deleteSongAction(_idOfSong));
    };

    return (
        <div className="h-100" style={showRegister ? registerBackStyle : {}}>
            {   !authState.authenticated &&
                    <div className="row align-items-center h-75 m-0">
                        <div className="container align-content-middle offset-sm-0 col-sm-12 offset-md-8 col-md-4 h-100">
                            <div className="row h-75" style={showRegister ? registerLogoStyle : {}} />
                            <div className="row h-25 pt-3">
                                <button className="offset-2 col-8 h-50 bg-transparent fw-bold themeColorsLightOutline" onClick={() => history.push("/user/register")}>
                                    Register
                                </button>
                            </div>
                        </div>
                    </div>
            }
            {   authState.authenticated &&
                    <div className="row-4 mt-5">
                        <ItemCards items={songState.artists} />
                        <ItemTable 
                            items={songState.songs} 
                            itemAttributes={attributes} 
                            itemType={"song"} 
                            putPlaylistSongs={addPlaylistSongs} 
                            inPlaylist={false}
                            deleteItem={deleteSong} 
                        />
                    </div>
            }
        </div>
    );
};

export default Home;