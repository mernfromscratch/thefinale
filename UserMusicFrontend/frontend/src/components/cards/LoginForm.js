import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginUserAction } from '../../redux/actions/authActions';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const LoginForm = ({ history }) => {

    const [loginCredentials, setLoginCredentials] = useState({ userName: "", password: "" });
    const [showYourPassword, setShowYourPassword] = useState(false);
    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    const changeCredentials = (e) => {
        setLoginCredentials({ ...loginCredentials, [e.target.name]: e.target.value });
    }

    const login = () => {
        dispatch(loginUserAction(loginCredentials));
    }

    useEffect(() => {
        authState.authenticated && history.push("/");
    }, [authState]);

    const handleMouseDownPassword = (e) => {
        e.preventDefault();
    };

    return (
        <div className="container-fluid d-flex align-items-center justify-content-center">
            <div className="row mt-5">
                <div className="col">
                    <Card>
                        <CardContent>
                            <div className="h5 mb-3">
                                Log in
                            </div>
                            <div>
                                <div className="col mt-3 mb-3">
                                    <TextField 
                                        fullWidth
                                        margin="normal"
                                        label="Your Username"
                                        type="text"
                                        name="userName" 
                                        value={loginCredentials.userName} 
                                        onChange={changeCredentials} 
                                    /> 
                                </div>
                            </div>
                            <div>
                                <div className="col mt-3 mb-3">
                                    <FormControl>
                                        <InputLabel htmlFor="password">Your password please</InputLabel>
                                        <Input
                                            id="password"
                                            type={showYourPassword ? 'text' : 'password'}
                                            value={loginCredentials.password}
                                            name="password"
                                            onChange={changeCredentials} 
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        onClick={() => setShowYourPassword(!showYourPassword)}
                                                        onMouseDown={handleMouseDownPassword}
                                                    >
                                                    {showYourPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                            </div>
                            <div className="d-flex justify-content-center">
                                <Button variant="contained" className="mb-1 mt-3" onClick={login}>
                                    Log in
                                </Button>
                            </div>
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>
    );
};

export default LoginForm;