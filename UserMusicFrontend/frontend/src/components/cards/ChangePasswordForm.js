import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { putPasswordAction } from '../../redux/actions/authActions';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const ChangePasswordForm = ({ history }) => {

    const [passwordCredentials, setPasswordCredentials] = useState({ password: "", newPassword: "" });
    const [showYourPassword, setShowYourPassword] = useState(false);
    const [showNewPassword, setShowNewPassword] = useState(false);
    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);
    const inputs = [ 
        {valueName: 'password', displayName: 'Your password'},
        {valueName: 'newPassword', displayName: 'New password'},
    ]

    const changeCredentials = (e) => {
        setPasswordCredentials({ ...passwordCredentials, [e.target.name]: e.target.value });
    }

    const sendPasswordToBack = () => {
        dispatch(putPasswordAction(passwordCredentials));
    }

    const handleMouseDownPassword = (e) => {
        e.preventDefault();
    };

    useEffect(() => {
        authState.message && (!authState.message.errorOccurred) && (authState.message.text === "Password successfully updated ^^") && history.push("/");
    }, [authState]);

    const inputRow = (passwordType) => {
        const { valueName, displayName } = passwordType;
        return  <div>
                    <FormControl>
                        <InputLabel htmlFor={valueName}>{displayName}</InputLabel>
                        <Input
                            id={valueName}
                            type={valueName === 'password' ? (showYourPassword ? 'text' : 'password') : (showNewPassword ? 'text' : 'password')}
                            name={valueName}
                            value={valueName === 'password' ? passwordCredentials.password : passwordCredentials.newPassword}
                            onChange={(e) => changeCredentials(e)} 
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => valueName === 'password' ? setShowYourPassword(!showYourPassword) : setShowNewPassword(!showNewPassword)}
                                        onMouseDown={handleMouseDownPassword}
                                    >
                                    {valueName === 'password' ? (showYourPassword ? <Visibility /> : <VisibilityOff />) : (showNewPassword ? <Visibility /> : <VisibilityOff />)}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </div>
    };

    return (
        <div className="container-fluid d-flex align-items-center justify-content-center">
            <div className="row mt-5">
                <div className="col">
                    <Card>
                        <CardContent>
                            { inputs.map((input => inputRow(input))) }
                            <div className="d-flex justify-content-center">
                                <Button variant="contained" className="mb-1 mt-3" onClick={sendPasswordToBack}>
                                    Submit
                                </Button>
                            </div>
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>
    );
};

export default ChangePasswordForm;