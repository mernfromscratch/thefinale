import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getSongAttributes, getArtistAttributes } from '../../helperFunctions/song';
import { 
    getAllSongsAction, getAllArtistsAction,
    postSongAction, postArtistAction, 
} from '../../redux/actions/songActions';

import ItemForm from "./ItemForm";

const ItemCreate = ({ history, location }) => {

    const [item, setItem] = useState({});
    const [itemAttributes, setItemAttributes] = useState([]);
    const dispatch = useDispatch();
    const { songState } = useSelector(state => state);
    const urlSplit = location.pathname.split('/');
    const nameOfItem = urlSplit[urlSplit.length - 1];

    const actionForCreate = { 
        song: postSongAction,
        artist: postArtistAction
     };

     const getAttributesForItem = {
        song: getSongAttributes,
        artist: getArtistAttributes
     };

     const getOptionsForModelSelect = {
        songs: getAllSongsAction,
        artists: getAllArtistsAction
     };

     useEffect(() => {
        getAttributesForItem[nameOfItem]()
            .then((attributes) => {
                attributes.forEach(attribute => {
                    attribute.type === "modelSelect" && dispatch(getOptionsForModelSelect[attribute.modelOptions]());
                });
                makeDefaultItem(attributes);
                setItemAttributes(attributes);
            })
     }, []);

     useEffect(() => {
        songState.message && !songState.message.errorOccurred && songState.message.text.includes("created") && history.push("/admin");
     }, [songState.message]);

     const create = () => {
        let itemToSend = {};
        if (nameOfItem === "song") {
            itemToSend = {
                ...item,
                songUrl: songState.uploadedMusicInfo ? songState.uploadedMusicInfo.url : null,
                cloudinarySongId: songState.uploadedMusicInfo ? songState.uploadedMusicInfo.publicId : null 
            };
        } else if (nameOfItem === "artist") {
            itemToSend = {
                ...item,
                pictureUrl: songState.uploadedImageInfo ? songState.uploadedImageInfo.url : null, 
                imageId: songState.uploadedImageInfo ? songState.uploadedImageInfo.publicId : null 
            };
        };
        dispatch(actionForCreate[nameOfItem](itemToSend));
     };

     const makeDefaultItem = (attributes) => {
        let defaultItem = {};
        attributes.forEach(attribute => {
            defaultItem[attribute.valueName] = attribute.default;
        });
        setItem(defaultItem);
    };

    return (
        <ItemForm item={item} setItem={setItem} itemAttributes={itemAttributes} title={"Create " + nameOfItem} sendToBack={create} />
    );
};

export default ItemCreate;