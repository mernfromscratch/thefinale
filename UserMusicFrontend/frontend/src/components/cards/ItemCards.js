import { useHistory } from 'react-router-dom';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    icon: {
      marginRight: theme.spacing(2),
    },
    heroContent: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
      marginTop: theme.spacing(4),
    },
    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    },
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      cursor: 'pointer'
    },
    cardMedia: {
      paddingTop: '56.25%', // 16:9
    },
    cardContent: {
      flexGrow: 1,
    },
    footer: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(6),
    },
  }));

const ItemCards = ({ items, itemAttributes }) => {
    const classes = useStyles();
    const history = useHistory();

    return (
        <Container className={classes.cardGrid} maxWidth="md">
            <Grid container spacing={4}>
              {items && items.map((item) => (
                  <Grid item key={item._id} sm={6} md={4}>
                      <Card className={classes.card} onClick={() => history.push(`/artist/${item._id}`)}>
                          <CardMedia
                          className={classes.cardMedia}
                          image={item["pictureUrl"] || "https://source.unsplash.com/random"}
                          title="Artist image"
                          />
                          <CardContent className={classes.cardContent}>
                              <Typography gutterBottom variant="h5" component="h2">
                                  { item.name || item.title }
                              </Typography>
                              <Typography>
                                  { item.details || item.genre }
                                  { item.artists && (item.artists[0] + item.artists.slice(1).reduce((allArtists, currArtist) => allArtists + " , " + currArtist)) }
                              </Typography>
                          </CardContent>
                      </Card>
                  </Grid>
              ))}
            </Grid>
      </Container>
    )
};

export default ItemCards;