import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getSongAttributes, getArtistAttributes } from '../../helperFunctions/song';
import { 
    getAllSongsAction, getAllArtistsAction,
    getSongForEditAction, getArtistForEditingAction,
    putSongAction, putArtistAction,
} from '../../redux/actions/songActions';

import ItemForm from "./ItemForm";

const ItemEdit = ({ history, location }) => {

    const [item, setItem] = useState({});
    const [itemAttributes, setItemAttributes] = useState([]);
    const dispatch = useDispatch();
    const { songState } = useSelector(state => state);
    const urlSplit = location.pathname.split('/');
    const nameOfItem = urlSplit[urlSplit.length - 2];
    const { _id } = useParams();

    const actionForEdit = { 
        song: putSongAction,
        artist: putArtistAction
    };

    const actionForFetch = {
        song: getSongForEditAction,
        artist: getArtistForEditingAction,
    };

    const getAttributesForItem = {
        song: getSongAttributes,
        artist: getArtistAttributes
    };

    const getOptionsForModelSelect = {
        songs: getAllSongsAction,
        artists: getAllArtistsAction
     };

    const edit = () => {
        let itemToSend = {...item};
        songState.uploadedImageInfo && (itemToSend = { ...item, pictureUrl: songState.uploadedImageInfo.url, imageId: songState.uploadedImageInfo.publicId });
        songState.uploadedMusicInfo && (itemToSend = { ...item, musicUrl: songState.uploadedMusicInfo.url, cloudinarySongId: songState.uploadedMusicInfo.publicId });
        dispatch(actionForEdit[nameOfItem](itemToSend));
    }

    useEffect(() => {
        dispatch(actionForFetch[nameOfItem](_id));
        getAttributesForItem[nameOfItem]()
            .then((attributes) => {
                attributes.forEach(attribute => {
                    attribute.type === "modelSelect" && dispatch(getOptionsForModelSelect[attribute.modelOptions]());
                });
                setItemAttributes(attributes);
            })
    }, []);

    useEffect(() => {
        songState.message && !songState.message.errorOccurred && songState.message.text.includes("updated") && history.push("/admin");
     }, [songState.message]);

    useEffect(() => {
        songState.songForEditing && setItem(songState.songForEditing);
        songState.artistForEditing && setItem(songState.artistForEditing);
    }, [songState.songForEditing, songState.artistForEditing]);

    return (
        <ItemForm item={item} setItem={setItem} itemAttributes={itemAttributes} title={"Edit " + nameOfItem + ": " + ((item && (item.name || item.title)) || "")} sendToBack={edit} />
    );
};

export default ItemEdit;