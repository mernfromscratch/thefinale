import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { 
    uploadImageAction, removeImageAction,
    uploadMusicAction, removeMusicAction,
} from '../../redux/actions/songActions';
import Resizer from 'react-image-file-resizer';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import Autocomplete from '@material-ui/lab/Autocomplete';

const ItemForm = ({ item, setItem, itemAttributes, title, sendToBack }) => {

    const { songState } = useSelector(state => state);
    const [ itemImageForDeletion, setItemImageForDeletion ] = useState(null);
    const [ itemMusicForDeletion, setItemMusicForDeletion ] = useState(null);
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        return () => {
            !songState.message && songState.uploadedImageInfo && dispatch(removeImageAction(songState.uploadedImageInfo.publicId));                 // Artist not uploaded, delete image if it exists
            !songState.message && songState.uploadedMusicInfo && dispatch(removeMusicAction(songState.uploadedMusicInfo.publicId));                 // Song not uploaded, delete music if it exists
            songState.message && !songState.message.errorOccurred && itemImageForDeletion && dispatch(removeImageAction(itemImageForDeletion));     // Artist updated and item img removed, delete item image
            songState.message && !songState.message.errorOccurred && itemMusicForDeletion && dispatch(removeMusicAction(itemMusicForDeletion));     // Song updated and item music removed, delete item music
        }
    }, [history]);

    useEffect(() => {
        // Because redux can be a little weird/buggy sometimes, we double check that no unused music/image is stored
        songState.uploadedImageInfo && dispatch(removeImageAction(songState.uploadedImageInfo.publicId));
        songState.uploadedMusicInfo && dispatch(removeMusicAction(songState.uploadedMusicInfo.publicId));
    }, []);

    useEffect(() => {
        // Comes into effect when one uploads two or more images/musics in a row
        songState.oldImageToDelete && dispatch(removeImageAction(songState.oldImageToDelete.publicId));
        songState.oldMusicToDelete && dispatch(removeMusicAction(songState.oldMusicToDelete.publicId));
    }, [songState.oldImageToDelete, songState.oldMusicToDelete]);

    const changeItem = (e) => {
        const value = e.target.type === "checkbox" ? !(item[e.target.name]) : e.target.value;
        setItem({ ...item, [e.target.name]: value });
    } 

    const changeSwitchItem = (e) => {
        setItem({ ...item, [e.target.name]: e.target.checked });
      };
    
    const uploadImage = (e) => {
        let image = e.target.files[0];
        image && Resizer.imageFileResizer(image, 1080, 360, 'JPEG', 100, 0, (uri) => {
            dispatch(uploadImageAction(uri));
        }, 'base64');    
    };

    const removeImage = () => {
        let imageId = (songState.uploadedImageInfo && songState.uploadedImageInfo.publicId) || item["imageId"];
        item["imageId"] && setItemImageForDeletion(item["imageId"]);
        item["imageId"] && setItem({ ...item, pictureUrl: null, imageId: null });
        !(imageId === item["imageId"]) && dispatch(removeImageAction(imageId));
    };

    const uploadMusic = (e) => {
        let music = e.target.files[0];
        const reader = new FileReader();
        if (music) {
            reader.onload = () => { dispatch(uploadMusicAction(reader.result)) }; 
            music && reader.readAsDataURL(music);       
        };
    };

    const removeMusic = () => {
        let musicId = (songState.uploadedMusicInfo && songState.uploadedMusicInfo.publicId) || item["cloudinarySongId"];
        item["cloudinarySongId"] && setItemMusicForDeletion(item["cloudinarySongId"]);
        item["cloudinarySongId"] && setItem({ ...item, songUrl: null, cloudinarySongId: null });
        !(musicId === item["cloudinarySongId"]) && dispatch(removeMusicAction(musicId)); 
    };

    const giveInput = (attribute) => {
        const { displayName, valueName, type, min, max, multiple, modelOptions, options, doNotShowInput } = attribute;
        const preSelectedModelOptions = (modelOptions && item && item[valueName] && songState[modelOptions].filter(option => item[valueName].filter(alreadySelectedOption => alreadySelectedOption._id === option._id).length > 0)) || [];
        const input = () => {
            if (!item || doNotShowInput) {
                return <></>
            } else if (type === "text" || type === "textarea" || type === "number") { 
                return  <TextField 
                            fullWidth
                            margin="normal"
                            label={displayName}
                            type={type}
                            multiline={type === "textarea" ? true : false}
                            name={valueName} 
                            value={item[valueName]} 
                            onChange={changeItem} 
                            InputLabelProps={{ shrink: true }}
                            inputProps={{ min, max }}
                        /> 
            } else if (type === "modelSelect" || type === "select") {
                if ( !(preSelectedModelOptions || (songState[modelOptions] || options)) ) {
                    return <></>
                };
                if (type === "modelSelect") {
                    const params = {
                        value: preSelectedModelOptions,
                        options: songState[modelOptions],
                        getOptionSelected: ((option, value) => option._id === value._id),
                        multiple, 
                        name: valueName, 
                        getOptionLabel: (option) => option.title || option.name || option,
                        onChange: (e, val) => setItem({ ...item, [valueName]: val }),
                        renderInput: (params) => (
                            <TextField
                                {...params}
                                variant="standard"
                                label={displayName}
                                placeholder={displayName}
                            />
                            )
                    };
                    return  <Autocomplete {...params} />
                } else if (type === "select") {
                    return  <div className="container-fluid">
                                <div className="row m-0">
                                    <Select
                                        style={{paddingLeft: "0px"}}
                                        value={item[valueName] || ""}
                                        label={valueName}
                                        name={valueName}
                                        onChange={changeItem}
                                    >
                                        {options.map((option) => (
                                            <MenuItem value={option}> {option} </MenuItem>
                                        ))}
                                    </Select>
                                </div>
                            </div>  
                    
               } else {
                    return <></>
                }
            } else if (type === "checkbox") {
                return  <div className="d-flex justify-content-center">
                            <FormControlLabel
                                control={<Switch checked={item[valueName]} onChange={changeSwitchItem} name={valueName} />}
                                label={displayName}
                            />
                        </div>
            } else if (type === "picture") {
                return  <div className="container">
                            <div className="row" style={{ display: ((songState.uploadedImageInfo || item[valueName]) ? "inline" : "none") }}>
                                <div className="d-flex justify-content-center">
                                    <Badge color="secondary" badgeContent="X" invisible={!songState.uploadedImageInfo && !item[valueName]} className="mr-3 btn" onClick={() => removeImage()}>
                                        <Avatar 
                                            alt="Uploaded image" 
                                            variant="square"
                                            style={{ display: (songState.uploadedImageInfo || item[valueName]) ? "block" : "none" }} 
                                            src={(songState.uploadedImageInfo && songState.uploadedImageInfo.url) || (item[valueName] && item[valueName])} 
                                        />
                                    </Badge>
                                </div>
                            </div>
                            <div className="row">
                                <div className="d-flex justify-content-center">
                                    <input
                                        accept="image/*"
                                        style={{ display: 'none' }}
                                        id="picture-input"
                                        type="file"
                                        onChange={uploadImage}
                                    />
                                    <label htmlFor="picture-input">
                                        <Button variant="outlined" component="span">
                                            Upload picture
                                        </Button>
                                    </label> 
                                </div>
                            </div>
                        </div>
            } else if (type === "music") {
                return  <div className="container-fluid">
                            <div className="row" >
                                <div className="d-flex justify-content-center mb-2">
                                    <Badge color="secondary" badgeContent="X" invisible={!songState.uploadedMusicInfo && !item[valueName]} className="mr-3 btn" onClick={() => removeMusic()}>
                                        <audio 
                                            controls
                                            name={valueName}
                                            alt="Uploaded music"
                                            style={{ display: (songState.uploadedMusicInfo || item[valueName]) ? "block" : "none" }}
                                            src={(songState.uploadedMusicInfo && songState.uploadedMusicInfo.url) || (item[valueName] && item[valueName])}
                                            onLoadedMetadata={(e) => setItem({ ...item, length: parseInt(e.target.duration) })}
                                        />
                                    </Badge>
                                </div>
                            </div>
                            <div className="row">
                                <div className="d-flex justify-content-center">
                                    <input
                                        accept="audio/*"
                                        style={{ display: 'none' }}
                                        id="audio-input"
                                        type="file"
                                        onChange={uploadMusic}
                                    />
                                    <label htmlFor="audio-input">
                                        <Button variant="outlined" component="span">
                                            Upload song file
                                        </Button>
                                    </label> 
                                </div>
                            </div>
                        </div>
            }
        }
        return (
            <div key={valueName}>
                <div className="col mt-3 mb-3">
                    {input()}
                </div>
            </div>
        );

    }

    return (
        <div className="container-fluid d-flex align-items-center justify-content-center h-100 overflow-auto">
            <div className="col-6 mt-5">
                <div className="h5 row-12">
                    { title }
                </div>
                <div className="themeColorsReverse row-12">
                    { !(item === {}) && itemAttributes.map((attribute) => giveInput(attribute)) }
                    <div className="row-6 d-flex justify-content-center">
                        <Button variant="contained" className="mb-4 mt-5" onClick={sendToBack}>
                            Submit
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ItemForm;