import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllArtistsAction, getAllSongsAction } from "../../redux/actions/songActions";

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const ChooseItemToEdit = ({ history }) => {

    const adminButtonStyle = { backgroundColor: "#FFF", color: "#000" };
    const editableItemTypes = ['songs', 'artists'];

    const [itemType, setItemType] = useState(null);
    const [listOfItems, setListOfItems] = useState([]);
    const itemToGet = { songs: getAllSongsAction, artists: getAllArtistsAction};
    const dispatch = useDispatch();
    const { songState } = useSelector(state => state);

    useEffect(() => {
        itemType && dispatch(itemToGet[itemType]());
    }, [itemType]);

    useEffect(() => {
        itemType === "songs" && songState.songs && setListOfItems(songState.songs);
        itemType === "artists" && songState.artists && setListOfItems(songState.artists);
    }, [songState.songs, songState.artists]);

    const sendToEditPage = (_id) => {
        const editPageToPushTo = "/edit/" + itemType.slice(0, -1) + "/" + _id;
        history.push(editPageToPushTo);
    };

    return (
        <div className="container align-items-center h-100 overflow-auto">
            {   !itemType &&
                    <div className="row h-100 align-items-center">
                        <div className="col-6 offset-3">
                            {editableItemTypes.map((itemName) => (
                                <div key={itemName} className="row mt-4">
                                    <Button variant="contained" size="large" style={adminButtonStyle} onClick={() => {setItemType(itemName)}}>
                                        {"Edit " + itemName.charAt(0).toUpperCase() + itemName.slice(1)}
                                    </Button>
                                </div>
                            ))}
                        </div>
                    </div>
            }

            {   itemType && listOfItems &&
                        <div className="row h-100 align-items-center">
                            <div className="col-4 offset-4">
                                <Autocomplete
                                    name={itemType.charAt(0).toUpperCase() + itemType.slice(1)}
                                    options={listOfItems}
                                    getOptionLabel={(option) => option.title || option.name}
                                    defaultValue={null || (listOfItems[0] && (listOfItems[0].title || listOfItems[0].name))}
                                    onChange={(e, val) => val && sendToEditPage(val._id)}
                                    renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant="standard"
                                        label={itemType.charAt(0).toUpperCase() + itemType.slice(1)}
                                        placeholder={"Choose " + itemType.slice(0, -1) + " to edit"}
                                    />
                                    )}
                                />
                            </div>
                        </div>
            }
        </div>
    );
};

export default ChooseItemToEdit;