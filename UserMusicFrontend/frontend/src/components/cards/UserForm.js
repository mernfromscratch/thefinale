import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';


import {
    registerUserAction,
    getCurrentUserAction,
    putUserAction,
    postAdminAction
} from '../../redux/actions/authActions';

import { getUserAttributes } from '../../helperFunctions/auth';

const UserForm = ({ history, location }) => {
    const [user, setUser] = useState({});
    const [password, setPassword] = useState("");
    const [showYourPassword, setShowYourPassword] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [attributes, setAttributes] = useState([]);
    const titlesForActions = { edit: "Change Your Info", register: "Register", create: "Create An Admin" };
    const actionToDispatch = { edit: putUserAction, register: registerUserAction };
    const action = location.pathname.split('/').pop();

    const dispatch = useDispatch();
    const { authState } = useSelector(state => state);

    useEffect(() => {
        if (authState.authenticated && (action === "edit")) { dispatch(getCurrentUserAction()) };
        getUserAttributes()
            .then((attributes) => {
                setAttributes(attributes);
                if (!(action === "edit")) {
                    let defaultUser = {};
                    attributes.map((attribute) => { defaultUser[attribute.valueName] = attribute.defaultValue });
                    setUser(defaultUser);
                };
            })
            .catch((error) => console.log(error));
    }, []);

    useEffect(() => {
        authState.authenticated && !(action === "create") && setUser(authState.currentUserToEdit);                              // If editing user, fetch user info
        authState.message && (action === "register") && !(authState.message.errorOccurred) && history.push("/user/login");      // If registering was successful, go to login page
    }, [authState]);

    const sendToBack = () => {
        !(action === "create") 
            ?   dispatch(actionToDispatch[action](user)) 
            :   dispatch(postAdminAction(user, password));
        //if (!(action === "edit")) { dispatch( clearCurrentUserInfoAction() ) }; 
    }

    const changeUser = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    }

    const handleMouseDownPassword = (e) => {
        e.preventDefault();
    };

    const makeRow = (attribute) => {
        const { displayName, valueName, type, min, max, onlyForRegister, onlyForMakingAdmin } = attribute;
        if ((action === ("edit")) && onlyForRegister) { return <></> };
        if (!(action === ("create")) && onlyForMakingAdmin) { return <></> };
        const input = () => {
            if (type === "text" || type === "number") { return  <TextField 
                                                                    fullWidth
                                                                    margin="normal"
                                                                    label={displayName}
                                                                    type={type}
                                                                    name={valueName} 
                                                                    value={user[valueName]} 
                                                                    onChange={changeUser} 
                                                                    inputProps={{ min, max }}
                                                                /> 
            } else if (type === "password") { return    <FormControl style ={{width: '100%'}}>
                                                            <InputLabel htmlFor={valueName}>{displayName}</InputLabel>
                                                            <Input
                                                                id={onlyForMakingAdmin ? `YourOwnPwd${valueName}` : `${valueName}`}
                                                                type={onlyForMakingAdmin ? (showYourPassword ? 'text' : 'password') : (showPassword ? 'text' : 'password')}
                                                                name={valueName}
                                                                value={onlyForMakingAdmin ? password : user[valueName]}
                                                                onChange={(e) => onlyForMakingAdmin ? setPassword(e.target.value) : changeUser(e)}
                                                                endAdornment={
                                                                    <InputAdornment position="end">
                                                                        <IconButton
                                                                            onClick={() => onlyForMakingAdmin ? setShowYourPassword(!showYourPassword) : setShowPassword(!showPassword)}
                                                                            onMouseDown={handleMouseDownPassword}
                                                                        >
                                                                        {onlyForMakingAdmin ? (showYourPassword ? <Visibility /> : <VisibilityOff />) : (showPassword ? <Visibility /> : <VisibilityOff />)}
                                                                        </IconButton>
                                                                    </InputAdornment>
                                                                }
                                                            />
                                                        </FormControl>
            }
        }
        return (
            <div key={onlyForMakingAdmin ? `${valueName}_making_admin` : valueName}>
                <div className="col mt-3 mb-3">
                    {input()}
                </div>
            </div>
        );
    };

    return (
        <div className="container-fluid d-flex align-items-center justify-content-center">
            <div className="row w-100 mt-5">
                <div className="offset-sm-1 col-sm-10 offset-md-4 col-md-4">
                    <Card>
                        <CardContent>
                            <div className="h5 mb-3">
                                {titlesForActions[action]}
                            </div>
                            {!(attributes === []) && user && attributes.map(attribute => makeRow(attribute))}
                            <div className="d-flex justify-content-center">
                                <Button variant="outlined" size="large" className="mb-1 mt-3" onClick={sendToBack}>
                                    Submit
                                </Button>
                            </div>
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>
    )
};

export default UserForm;