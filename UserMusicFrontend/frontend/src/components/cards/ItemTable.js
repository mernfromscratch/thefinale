import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getPlaylistAttributes, makeTimeHumanFriendly } from '../../helperFunctions/song';
import { continueSongAction, pauseSongAction, playNewSongAction } from '../../redux/actions/songActions';

import ItemForm from "../cards/ItemForm";

import { Dialog } from "@material-ui/core";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';

const ItemTable = ({ items, itemAttributes, itemType, putPlaylistSongs, playlistRelated, inPlaylist, deleteItem }) => {


    const dispatch = useDispatch();
    const { songState, authState } = useSelector(state => state);
    const history = useHistory();
    const [playlistAttributes, setPlaylistAttributes] = useState([]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [itemForMenu, setItemForMenu] = useState(null);
    const [anchorEl, setAnchorEl] = useState(null);
    const [menuOpen, setMenuOpen] = useState(false);
    const [addToPlaylistAnchorEl, setaddToPlaylistAnchorEl] = useState(null);
    const [itemIdForAddToPlaylistAnchor, setItemIdForAddToPlaylistAnchor] = useState(null);
    const [menuForAddingToPlaylistOpen, setMenuForAddingToPlaylistOpen] = useState(false);
    const ITEM_HEIGHT = 48;

    useEffect(() => {
        anchorEl && (anchorEl.name === "playlistMenu") && setMenuOpen(true);
        itemForMenu && setMenuOpen(true);
    }, [anchorEl, itemForMenu]);

    const dialogPopup = () => {
        getPlaylistAttributes().then((attributes) => setPlaylistAttributes(attributes));
        handleMenuClose();
        setDialogOpen(true);
    };

    const dialogMenu = () => (
        playlistRelated && (
            <Dialog fullWidth={true} maxWidth={"xs"} open={dialogOpen} onClose={() => setDialogOpen(false)}>
                <ItemForm 
                    item={playlistRelated.playlistEditing} 
                    setItem={playlistRelated.setPlaylistEditing} 
                    itemAttributes={playlistAttributes} 
                    title={(playlistRelated.playlistViewing && ("Edit" + playlistRelated.playlistViewing.name)) || "Edit playlist"} 
                    sendToBack={playlistRelated.putPlaylist} 
                />
            </Dialog>
        )
    );

    const handleMenuClick = (event, item) => {
        if (!anchorEl) {
            setAnchorEl(event.currentTarget);
            item && setItemForMenu(item);
        } else {
            setAnchorEl(null);
        }
    };
    
    const handleMenuClose = () => {
        setMenuOpen(false);
        setAnchorEl(null);
        setItemForMenu(null);
        handleAddToPlaylistMenuClose();
    };

    const handleAddToPlaylistMenuClick = (event) => {
        setaddToPlaylistAnchorEl(event.currentTarget);
        setItemIdForAddToPlaylistAnchor(event.currentTarget.id);
        setMenuForAddingToPlaylistOpen(true);
    };

    const handleAddToPlaylistMenuClose = () => {
        setaddToPlaylistAnchorEl(null);
        setItemIdForAddToPlaylistAnchor(null);
        setMenuForAddingToPlaylistOpen(false);
    };

    const handleMenuOption = (option, item, playlistToAddOrRemoveTo) => {
        handleMenuClose();
        (option === "edit") && history.push(`/edit/${itemType}/${item._id}`);
        (option === "editPlaylist") && dialogPopup();
        (option === "removeFromPlaylist") && putPlaylistSongs(item, "remove");
        (option === "addToPlaylist") && putPlaylistSongs(item, "add", playlistToAddOrRemoveTo);
        (option === "delete") && deleteItem(item._id);
    };

    const menuOptionsAdmin = (item) => {
        let optionsAdmin = [];
        optionsAdmin.push(  
            <MenuItem key={`edit_${item._id}`} onClick={() => handleMenuOption("edit", item)}>
                Edit
            </MenuItem>
        );
        optionsAdmin.push(
            <MenuItem key={`delete_${item._id}`} onClick={() => handleMenuOption("delete", item)}>
                Delete
            </MenuItem>
        );
        return optionsAdmin;
    };

    const menuOptionsSong = (item) => {
        let optionsSong = [];
        optionsSong.push(
            <MenuItem id={`submenu_${item._id}`} onClick={handleAddToPlaylistMenuClick}>
                Add to playlist
            </MenuItem>
        );
        inPlaylist && playlistRelated.isOwnerOfPlaylist() && optionsSong.push(
            <MenuItem onClick={() => handleMenuOption("removeFromPlaylist", item)}>
                Remove from playlist
            </MenuItem>
        );
        return optionsSong;
    };

    const menuOptionsPlaylist = () => {
        let optionsPlaylist = [];
        optionsPlaylist.push(
            <MenuItem key={`edit`} onClick={() => handleMenuOption("editPlaylist", null, null)}>
                Edit
            </MenuItem>
        );
        optionsPlaylist.push(
            <MenuItem key={`delete`} onClick={() => playlistRelated.deletePlaylist()}>
                Delete
            </MenuItem>
        );
        return optionsPlaylist;
    }

    const showModelSelect = (itemAttributeValue, attribute) => {
        let modelString = "";
        if (attribute.multiple) {
            modelString = itemAttributeValue[0] && (itemAttributeValue[0].name || itemAttributeValue[0].title);
            let allArtistsExceptFirst = "";
            (itemAttributeValue.length > 1) && (allArtistsExceptFirst = itemAttributeValue.slice(1).reduce((previousSelectVal, currentSelectVal) => previousSelectVal + "," + (currentSelectVal.name || currentSelectVal.title), allArtistsExceptFirst));
            modelString += allArtistsExceptFirst;
        } else {
            modelString = itemAttributeValue.name || itemAttributeValue.title;
        }
        return modelString;
    };

    const playSong = (songToPlay) => {
        if (songState.songPlaying === songToPlay && songState.playlistPlayingSongsFrom === songState.playlistSongs) { dispatch(continueSongAction()) }
        else { dispatch(playNewSongAction(songToPlay)) };
    };

    const isPlayingThisPlaylistAndSong = (songToPlay) => {
        return (songState.playlistPlayingSongsFrom === songState.playlistSongs && songState.songPlaying === songToPlay && songState.play);
    };

    const isPlayingThisPlaylist = () => {
        return (songState.playlistPlayingSongsFrom === songState.playlistSongs && songState.play);
    };

    const menuTop = () => (
        <>
            <div className="row m-auto">
                <div className="col d-flex flex-row themeColorsReverse">
                    <IconButton onClick={() => isPlayingThisPlaylist() ? dispatch(pauseSongAction()) : playSong(songState.playlistSongs[0])}>
                        { !isPlayingThisPlaylist()
                            ?   <PlayArrowIcon style={{ fontSize: 40 }} />
                            :   <PauseIcon style={{ fontSize: 40 }} />
                        }
                    </IconButton>
                    { inPlaylist && playlistRelated.isOwnerOfPlaylist() &&
                        <IconButton name="playlistMenu" onClick={(e) => handleMenuClick(e, null)} >
                            <MoreHorizIcon/>
                        </IconButton>
                    }
                </div>
            </div>
        </>
    );

    const itemRow = (item) => {
        return  <TableRow key={item._id}>
                    <TableCell>
                        <IconButton onClick={() => isPlayingThisPlaylistAndSong(item) ? dispatch(pauseSongAction()) : playSong(item)}>
                            { !isPlayingThisPlaylistAndSong(item)
                                ?   <PlayArrowIcon />
                                :   <PauseIcon />
                            }
                        </IconButton>
                    </TableCell>
                    {itemAttributes.map(attribute => (
                        ((!attribute.adminOnly || authState.admin) && !attribute.onlyForCreateOrEdit) && 
                            <TableCell key={attribute.valueName}>
                                { attribute.type === "checkbox" 
                                    ? (item[attribute.valueName] ? "True" : "False") 
                                    : attribute.type === "modelSelect" 
                                        ? showModelSelect(item[attribute.valueName], attribute)
                                        : attribute.valueName === "length"
                                            ? makeTimeHumanFriendly(item[attribute.valueName])
                                            : (item[attribute.valueName]) 
                                }
                            </TableCell>
                    ))}
                    <TableCell>
                        <IconButton name={item._id} onClick={(e) => handleMenuClick(e, item)}>
                            <MoreHorizIcon />
                        </IconButton>
                    </TableCell>
                </TableRow>
    };

    const itemHeaderAttribute = (attribute) => {
        if ((!attribute.adminOnly || authState.admin) && !attribute.onlyForCreateOrEdit) {
            return  <TableCell key={attribute.valueName}>
                        {attribute.displayName}
                    </TableCell>
        }
    }

    return (
        <>
            <div className="container-fluid">
                { menuTop() }
            </div>
            <div>
            <Menu
                    anchorEl={anchorEl}
                    keepMounted
                    open={menuOpen}
                    onClose={handleMenuClose}
                    PaperProps={{
                        style: {
                            maxHeight: ITEM_HEIGHT * 4.5,
                            width: '20ch',
                        },
                    }}
                >
                    { itemType === "song" && anchorEl && itemForMenu && menuOptionsSong(itemForMenu) }
                    { authState.admin && itemForMenu && menuOptionsAdmin(itemForMenu) }
                    { anchorEl && anchorEl.name === "playlistMenu" && menuOptionsPlaylist() }
                </Menu>
                <Menu
                    anchorEl={addToPlaylistAnchorEl}
                    keepMounted
                    open={menuForAddingToPlaylistOpen}
                    onClose={handleMenuClose}
                    PaperProps={{
                        style: {
                            maxHeight: ITEM_HEIGHT * 4.5,
                            width: '20ch',
                        },
                    }}
                >
                    {songState.playlists.map(playlist => (
                        <MenuItem key={playlist._id} onClick={() => handleMenuOption("addToPlaylist", itemForMenu, playlist)}>
                            {playlist.name}
                        </MenuItem>
                    ))}
                </Menu>
            </div>
            {dialogMenu()}
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell />
                            { itemAttributes.map(attribute => itemHeaderAttribute(attribute)) }
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {items.map(item => (
                            itemRow(item)
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
};

export default ItemTable;